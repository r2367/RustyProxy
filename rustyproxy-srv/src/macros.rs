#[macro_export]
macro_rules! wrap {
    ($s: expr, $cert: expr, $pair: expr, $ca_cert: expr) => {{
        let mut acceptor = SslAcceptor::mozilla_intermediate(SslMethod::tls())?;
        acceptor.set_private_key($pair)?;
        acceptor.set_certificate($cert)?;
        acceptor.add_extra_chain_cert($ca_cert)?;
        acceptor.set_verify(SslVerifyMode::NONE);
        acceptor.check_private_key()?;
        let acceptor = acceptor.build();
        let ssl = Ssl::new(acceptor.context())?;
        let sslstream = SslStream::new(ssl, &$s)?;

        sslstream
    }};
}

#[macro_export]
macro_rules! is_auth {
    ($req: expr, $shared_secret: expr) => {{
        let mut res = false;
        for h in $req.headers() {
            if (h.name().as_str() == "rp_auth")
                && h.value() == &format!("{}", $shared_secret)
            {
                res = true;
                break;
            }
        }
        res
    }};
}