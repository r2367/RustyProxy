use log::debug;

use nomhttp::http_header::HttpHeader;
use nomhttp::http_response_status::HttpResponseStatus;
use nomhttp::http_version::HttpVersion;

use nom::bytes::streaming::{tag, take, take_until};
use nom::combinator::{iterator, map_res};
use nom::error::Error as NError;
use nom::sequence::terminated;
use nom::IResult;

use std::{error::Error, io::Read, str::FromStr};

use nom_bufreader_rp::bufreader::BufReader;
use nom_bufreader_rp::Parse;

pub fn parse_status_version(i: &[u8]) -> IResult<&[u8], HttpVersion, ()> {
    debug!("Entering parse_status_version: {:?}", i);
    let (i, version_au8) = take_until(" ")(i)?;
    let (_, version_s) = map_res(take(version_au8.len()), std::str::from_utf8)(version_au8)?;
    let version = HttpVersion::from_str(version_s).or(Err(nom::Err::Failure(())))?;

    let (i, _) = take(1u8)(i)?; // take the ' '
    Ok((i, version))
}

pub fn parse_status_code(i: &[u8]) -> IResult<&[u8], usize, ()> {
    debug!("Entering parse_status_code: {:?}", i);
    let (i, code_au8) = take_until(" ")(i)?;
    let (_, code_s) = map_res(take(code_au8.len()), std::str::from_utf8)(code_au8)?;
    let code = code_s.parse::<usize>().or(Err(nom::Err::Failure(())))?;

    let (i, _) = take(1u8)(i)?; // take the ' '
    Ok((i, code))
}

pub fn parse_status_text(i: &[u8]) -> IResult<&[u8], Vec<u8>, ()> {
    debug!("Entering parse_status_text: {:?}", i);
    let (i, msg_au8) = take_until("\r\n")(i)?;
    let msg: Vec<u8> = msg_au8.to_vec();

    let (i, _) = take(2u8)(i)?; // take the '\r\n'

    Ok((i, msg))
}

pub fn parse_status_line(i: &[u8]) -> IResult<&[u8], HttpResponseStatus, ()> {
    debug!("Entering parse_status_line: {:?}", i);
    let (i, version) = parse_status_version(i)?;
    let (i, code) = parse_status_code(i)?;
    let (i, msg) = parse_status_text(i)?;

    let rstatus = HttpResponseStatus::new(version, code, msg);
    Ok((i, rstatus))
}

pub fn parse_header(i: &[u8]) -> IResult<&[u8], HttpHeader, ()> {
    let (value_au8, name_au8) = take_until(": ")(i)?;
    let name = name_au8.iter().map(|&b| b as char).collect();
    let (value_au8, _) = take(2u8)(value_au8)?;
    let value: String = value_au8.iter().map(|&b| b as char).collect();

    let header = HttpHeader::new(name, value);

    Ok((value_au8, header))
}

pub fn parse_headers(i: &[u8]) -> IResult<&[u8], Vec<HttpHeader>, ()> {
    debug!("Entering parse_headers: {:?}", i);
    let (rest, headers_au8) = terminated(take_until("\r\n\r\n"), tag("\r\n"))(i)?;
    let headers_au8 = [headers_au8, rest].concat();

    let mut headers_iter = iterator(
        &headers_au8[..],
        terminated(take_until("\r\n"), tag::<_, _, NError<_>>("\r\n")),
    );

    let headers: Vec<HttpHeader> = headers_iter
        .map(parse_header)
        .filter_map(|r| r.ok())
        .map(|v| v.1)
        .collect();

    debug!("Headers: {:?}", headers);

    Ok((&rest[2..], headers)) // avoid last \r\n
}

fn nom_take_one(i: &[u8]) -> IResult<&[u8], u8, ()> {
    let (i, o) = take(1u8)(i)?;
    Ok((i, o[0]))
}

pub fn read_until_size<T>(reader: &mut BufReader<T>, s: u64) -> Result<Vec<u8>, Box<dyn Error>>
where
    T: Read,
{
    let mut out: Vec<u8> = Vec::with_capacity(s as usize);

    let mut bytes_read = 0;

    while bytes_read < s {
        let b = reader
            .parse(nom_take_one)
            .or(Err("Error during read_until_size parsing"))?;
        out.push(b);
        bytes_read += 1;
    }
    debug!("read_until_size: {} [Wanted: {}]", bytes_read, s);
    Ok(out)
}
