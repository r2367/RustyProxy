use chrono::Local;
use log::debug;
use nomhttp::http_method::HttpMethod;
use nomhttp::http_request::HttpRequest;
use nomhttp::http_response::HttpResponse;
use rusqlite::{Connection, DatabaseName};
use std::error::Error;
use std::fmt;
use std::fmt::Write as _;
use std::io::Write;
use std::sync::mpsc;
use std::time::Duration;

use crate::config::Config;
use crate::history::HistLine;
use crate::inspector::Inspector;
use crate::note::Note;
use crate::websockets::WebSockets;

#[derive(Debug)]
pub enum Speaker {
    Client,
    Server,
}

#[derive(Debug)]
pub enum DataType {
    History(usize),
    Inspector(usize),
    Websocket(usize),
    Note(usize),
}

#[derive(Debug)]
pub enum DataResult {
    History(Vec<HistLine>),
    Inspector(Vec<Inspector>),
    Websocket(Vec<WebSockets>),
    Note(Vec<Note>),
}

#[derive(Debug)]
pub enum PushDataType {
    Inspector(Inspector),
    Note(Note),
}

#[derive(Debug)]
pub enum Msg {
    WebSocketData(String, Speaker, bool, Vec<u8>),
    GiveMeData(DataType, mpsc::Sender<DataResult>),
    WriteNewData(PushDataType),
    WebData(HttpRequest, HttpResponse, String, bool, Duration),
    None,
}

type InspectorBfResultType = (
    usize,
    std::string::String,
    std::string::String,
    std::string::String,
    std::string::String,
    std::time::Duration
);
impl fmt::Display for Speaker {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let repr = match self {
            Speaker::Client => "Client",
            Speaker::Server => "Server",
        };
        write!(f, "{}", repr)
    }
}

fn _handle_sqlite(
    config: Config,
    receiver: mpsc::Receiver<Msg>,
    receiver_api: mpsc::Receiver<Msg>,
) -> Result<(), Box<dyn Error>> {
    let conn = Connection::open(format!("{}/hist.db", config.directory()))?;

    loop {
        let msg = if let Ok(msg) = receiver_api.try_recv() {
            msg
        } else if let Ok(msg) = receiver.try_recv() {
            msg
        } else {
            Msg::None
        };

        match msg {
            Msg::WebSocketData(direction, speaker, ssl, data) => {
                let speaker = format!("{}", speaker);
                let ssl = match ssl {
                    true => "1".to_string(),
                    false => "0".to_string(),
                };

                let cmd = format!("INSERT INTO websockets (stream_rep, speaker, ssl, data) VALUES (?1, ?2, ?3, ZEROBLOB({}))", data.len());
                conn.execute(&cmd, [direction, speaker, ssl])?;
                let row_id = conn.last_insert_rowid();
                let mut blob =
                    conn.blob_open(DatabaseName::Main, "websockets", "data", row_id, false)?;
                blob.write_all(&data)?;
            }
            Msg::WebData(req, resp, remote_addr, ssl, response_time) => {
                let uri = req.path();
                let now = format!("{}", Local::now().format("%H:%M:%S (%d/%m/%Y)"));
                let method = format!("{}", req.method());
                let size = format!("{}", req.as_bytes().len());
                let content_length = if let Some(content_length_header) = req
                    .headers()
                    .iter()
                    .find(|&x| x.name().to_lowercase() == "content-length")
                {
                    if let Ok(v) = content_length_header.value().parse::<usize>() {
                        v
                    } else {
                        0
                    }
                } else {
                    0
                }
                .to_string();

                let raw = String::from_utf8_lossy(&req.as_bytes()).to_string();
                let ssl = match ssl {
                    true => "1".to_string(),
                    false => "0".to_string(),
                };
                let raw_resp = resp.as_bytes();
                let status = format!("{}", resp.status().code());
                let params = {
                    match req.method() {
                        HttpMethod::Post | HttpMethod::Put | HttpMethod::Patch => "1".to_string(),
                        _ => {
                            if req.path().contains('?') {
                                "1".to_string()
                            } else {
                                "0".to_string()
                            }
                        }
                    }
                };
                let raw_resp_time = format!("{:?}", response_time);
                let cmd = format!("INSERT INTO history (uri, remote_addr, method, size, params, status, raw, ssl, response, response_time, timestamp, content_length) VALUES (?1, ?9, ?2, ?3, ?4, ?5, ?6, ?7, ZEROBLOB({}), ?8, ?10, ?11)", raw_resp.len());
                conn.execute(
                    &cmd,
                    [
                        uri,
                        &method,
                        &size,
                        &params,
                        &status,
                        &raw,
                        &ssl,
                        &raw_resp_time,
                        &remote_addr,
                        &now,
                        &content_length,
                    ],
                )?;
                let row_id = conn.last_insert_rowid();
                let mut blob =
                    conn.blob_open(DatabaseName::Main, "history", "response", row_id, false)?;
                blob.write_all(&raw_resp)?;
            }
            Msg::GiveMeData(datatype, sender) => match datatype {
                DataType::Note(id) => {
                    let mut stmt =
                        conn.prepare("SELECT id, body, title FROM notes WHERE id > ?")?;

                    let rows = stmt.query_map([id], |row| {
                        let note = Note {
                            id: row.get(0)?,
                            body: row.get(1)?,
                            title: row.get(2)?,
                        };
                        Ok(note)
                    })?;
                    let result = DataResult::Note(rows.collect::<Result<Vec<Note>, _>>()?);
                    sender.send(result)?;
                }
                DataType::Websocket(id) => {
                    let mut stmt = conn.prepare(
                        "SELECT id, stream_rep, speaker, ssl, data FROM websockets WHERE id > ?",
                    )?;

                    let rows = stmt.query_map([id], |row| {
                        let ssl: usize = row.get(3)?;
                        let data: Vec<u8> = row.get(4)?;
                        let mut data_string = String::new();

                        for b in data {
                            write!(data_string, "{}", b as char).unwrap();
                        }

                        let w = WebSockets {
                            id: row.get(0)?,
                            stream_rep: row.get(1)?,
                            speaker: row.get(2)?,
                            ssl: ssl == 1,
                            data: data_string,
                        };
                        Ok(w)
                    })?;
                    let result =
                        DataResult::Websocket(rows.collect::<Result<Vec<WebSockets>, _>>()?);
                    sender.send(result)?;
                }
                DataType::History(id) => {
                    let mut stmt = if config.is_paging_enabled() {
                        conn.prepare(
                            "SELECT * FROM history WHERE id > ? ORDER BY id Asc LIMIT 100",
                        )?
                    } else {
                        conn.prepare("SELECT * FROM history WHERE id > ? ORDER BY id Asc")?
                    };
                    let rows = stmt.query_map([id], |row| {
                        let data: Vec<u8> = row.get(10)?;
                        let mut data_string = String::new();
                        for b in data {
                            write!(data_string, "{}", b as char).unwrap();
                        }

                        let res = HistLine {
                            id: row.get(0)?,
                            remote_addr: row.get(1)?,
                            uri: row.get(2)?,
                            method: row.get(3)?,
                            size: row.get(6)?,
                            params: matches!(row.get(4)?, 1),
                            status: row.get(5)?,
                            raw: row.get(8)?,
                            ssl: row.get(9)?,
                            response: data_string,
                            response_time: row.get(11)?,
                            host: row
                                .get::<usize, String>(8)?
                                .split("ost: ")
                                .skip(1)
                                .take(1)
                                .collect::<String>()
                                .split("\r\n")
                                .take(1)
                                .collect::<String>(),
                            timestamp: row.get(7)?,
                            content_length: row.get(12)?,
                        };
                        Ok(res)
                    })?;
                    let ret = if config.is_scope_enabled() {
                        rows.filter(|x| {
                            x.is_ok()
                                && config.is_scope_enabled()
                                && x.as_ref().unwrap().host.contains(config.scope())
                        })
                        .collect::<Result<Vec<HistLine>, _>>()?
                    } else {
                        rows.collect::<Result<Vec<HistLine>, _>>()?
                    };
                    sender.send(DataResult::History(ret))?;
                }
                DataType::Inspector(id) => {
                    let mut stmt = conn.prepare("SELECT * FROM inspectors WHERE id > ?")?;
                    let rows = stmt.query_map([id], |row| {
                        let json: String = row.get(7)?;
                        let bf_results = match serde_json::from_str(json.as_str()) {
                            Ok(s) => Ok::<Vec<InspectorBfResultType>, rusqlite::Error>(s),
                            Err(_) => Ok::<Vec<InspectorBfResultType>, rusqlite::Error>(vec![(
                                0_usize,
                                "".to_string(),
                                "".to_string(),
                                "".to_string(),
                                "".to_string(),
                                Duration::from_secs(0)
                            )]),
                        }?;

                        let ssl: usize = row.get(5)?;

                        let i = Inspector {
                            id: row.get(0)?,
                            request: row.get(1)?,
                            response: row.get(2)?,
                            modified_request: row.get(3)?,
                            new_response: row.get(4)?,
                            ssl: ssl == 1,
                            target: row.get(6)?,
                            bf_results,
                            bf_request: row.get(8)?,
                        };
                        Ok(i)
                    })?;

                    sender.send(DataResult::Inspector(
                        rows.collect::<Result<Vec<Inspector>, _>>()?,
                    ))?;
                }
            },
            Msg::WriteNewData(pushdata) => match pushdata {
                PushDataType::Inspector(insp) => {
                    let ssl = if insp.ssl {
                        "1".to_string()
                    } else {
                        "0".to_string()
                    };
                    let mut stmt = conn.prepare("INSERT INTO inspectors(request, response, modified_request, new_response, ssl, target, bf_results, bf_request) VALUES(?, ?, ?, ?, ?, ?, ?, ?) RETURNING id")?;
                    let _i = stmt.query_row(
                        [
                            insp.request,
                            insp.response,
                            insp.modified_request,
                            insp.new_response,
                            ssl,
                            insp.target,
                            serde_json::to_string(&insp.bf_results)?,
                            insp.bf_request,
                        ],
                        |row| {
                            let i: usize = row.get(0)?;
                            Ok(i)
                        },
                    )?;
                }
                PushDataType::Note(note) => {
                    if note.id == 0 {
                        let mut stmt = conn
                            .prepare("INSERT INTO notes(title, body) VALUES(?, ?) returning id")?;
                        let _i = stmt.query_row([note.title, note.body], |row| {
                            let i: usize = row.get(0)?;
                            Ok(i)
                        })?;
                    } else {
                        let mut stmt = conn
                            .prepare("UPDATE notes SET title=?, body=? WHERE id=? returning id")?;
                        let _i =
                            stmt.query_row([note.title, note.body, note.id.to_string()], |row| {
                                let i: usize = row.get(0)?;
                                Ok(i)
                            });
                        debug!("SQLITE UPDATE: {:?}", _i);
                        _i?;
                    }
                }
            },
            Msg::None => {
                std::thread::sleep(std::time::Duration::from_millis(10));
            }
        }
    }
}

pub fn handle_sqlite(
    config: Config,
    receiver: mpsc::Receiver<Msg>,
    receiver_api: mpsc::Receiver<Msg>,
) -> bool {
    _handle_sqlite(config, receiver, receiver_api).is_ok()
}
