#[derive(serde::Deserialize, serde::Serialize, Debug)]
pub struct Note {
    pub id: usize,
    pub body: String,
    pub title: String,
}