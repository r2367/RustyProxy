#[derive(serde::Deserialize, serde::Serialize, Debug)]
pub struct WebSockets {
    pub id: usize,
    pub stream_rep: String,
    pub speaker: String,
    pub ssl: bool,
    pub data: String,
}