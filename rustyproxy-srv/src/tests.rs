/*
use crate::api_manager::handle_api_route;
use crate::api_response::ApiResponseStatus;
use crate::config::Config;
use crate::inspector::Inspector;
use nomhttp::http_header::HttpHeader;
use nomhttp::http_method::HttpMethod;
use nomhttp::http_request::HttpRequest;
use nomhttp::http_version::HttpVersion;
use rusqlite::Connection;
use std::fs;
use std::path::Path;
use std::sync::Once;

static INIT: Once = Once::new();

fn initialize() {
    INIT.call_once(|| {
        match Path::new("./test_project/").exists() {
            true => {}
            false => {
                fs::create_dir("./test_project/").unwrap();
            }
        }
        let conn = Connection::open("./test_project/hist.db").unwrap();
        conn.execute("DROP TABLE IF EXISTS history", []).unwrap();
        conn.execute(
            "CREATE TABLE history(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            remote_addr TEXT,
            uri TEXT,
            method TEXT,
            params INTEGER,
            status INTEGER,
            size INTEGER,
            raw BLOB,
            ssl INTEGER,
            response TEXT,
            response_time TEXT)",
            [],
        ).unwrap();

        conn.execute("DROP TABLE IF EXISTS inspectors", []).unwrap();
        conn.execute(
            "CREATE TABLE inspectors(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            request BLOB,
            response BLOB,
            modified_request BLOB,
            new_response BLOB,
            ssl INTEGER,
            target TEXT,
            bf_results BLOB,
            bf_request BLOB)",
            [],
        ).unwrap();

        conn.execute(
            "INSERT INTO inspectors(request, response, modified_request, new_response, ssl, target, bf_results, bf_request) VALUES(
                \"\", \"\", \"\", \"\", 0, \"\", \"\", \"\"
            )",
            [],
        ).unwrap();
        conn.execute(
            "INSERT INTO history (uri, remote_addr, method, size, params, status, raw, ssl, response, response_time) VALUES('', '', '', '', '', '', '', '', '', '')",
            [],
        ).unwrap();
    });
}

#[test]
fn get_api_bad_auth_generates_403() {
    let r = HttpRequest::new(
        HttpMethod::Get,
        "/api/test".to_string(),
        HttpVersion::Http11,
        vec![],
        vec![],
        vec![],
        true,
    );

    let cfg = Config::default();
    if let Ok(res) = handle_api_route(&r, cfg) {
        assert_eq!(res.status(), &ApiResponseStatus::Forbidden);
    }
}

#[test]
fn get_api_random_good_auth_generates_404() {
    let auth = HttpHeader::new("rp_auth".to_string(), "tests".to_string());
    let r = HttpRequest::new(
        HttpMethod::Get,
        "/api/test".to_string(),
        HttpVersion::Http11,
        vec![auth],
        vec![],
        vec![],
        true,
    );
    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");

    if let Ok(res) = handle_api_route(&r, cfg) {
        assert_eq!(res.status(), &ApiResponseStatus::NotFound);
    }
}

#[test]
fn get_api_requests_good_auth_good_id_generates_200() {
    initialize();

    let auth = HttpHeader::new("rp_auth".to_string(), "tests".to_string());
    let r = HttpRequest::new(
        HttpMethod::Get,
        "/api/requests/0".to_string(),
        HttpVersion::Http11,
        vec![auth],
        vec![],
        vec![],
        true,
    );
    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");

    if let Ok(res) = handle_api_route(&r, cfg) {
        assert_eq!(res.status(), &ApiResponseStatus::Ok);
    }
}

#[test]
fn get_api_requests_good_auth_bad_id_generates_404() {
    initialize();

    let auth = HttpHeader::new("rp_auth".to_string(), "tests".to_string());
    let r = HttpRequest::new(
        HttpMethod::Get,
        "/api/requests/123456".to_string(),
        HttpVersion::Http11,
        vec![auth],
        vec![],
        vec![],
        true,
    );
    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");

    if let Ok(res) = handle_api_route(&r, cfg) {
        assert_eq!(res.status(), &ApiResponseStatus::Ok);
    }
}

#[test]
fn get_api_requests_bad_auth_generates_403() {
    let r = HttpRequest::new(
        HttpMethod::Get,
        "/api/requests/0".to_string(),
        HttpVersion::Http11,
        vec![],
        vec![],
        vec![],
        true,
    );
    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");

    if let Ok(res) = handle_api_route(&r, cfg) {
        assert_eq!(res.status(), &ApiResponseStatus::Forbidden);
    }
}

#[test]
fn del_api_requests_good_auth_no_id_generates_404() {
    initialize();

    let auth = HttpHeader::new("rp_auth".to_string(), "tests".to_string());
    let r = HttpRequest::new(
        HttpMethod::Delete,
        "/api/requests/".to_string(),
        HttpVersion::Http11,
        vec![auth],
        vec![],
        vec![],
        true,
    );
    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");

    if let Ok(res) = handle_api_route(&r, cfg) {
        assert_eq!(res.status(), &ApiResponseStatus::NotFound);
    }
}

#[test]
fn del_api_requests_good_auth_bad_id_generates_404() {
    initialize();

    let auth = HttpHeader::new("rp_auth".to_string(), "tests".to_string());
    let r = HttpRequest::new(
        HttpMethod::Delete,
        "/api/requests/123456".to_string(),
        HttpVersion::Http11,
        vec![auth],
        vec![],
        vec![],
        true,
    );
    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");

    if let Ok(res) = handle_api_route(&r, cfg) {
        assert_eq!(res.status(), &ApiResponseStatus::NotFound);
    }
}

#[test]
fn del_api_requests_good_auth_good_id_generates_200() {
    initialize();

    let auth = HttpHeader::new("rp_auth".to_string(), "tests".to_string());
    let r = HttpRequest::new(
        HttpMethod::Delete,
        "/api/requests/1".to_string(),
        HttpVersion::Http11,
        vec![auth],
        vec![],
        vec![],
        true,
    );
    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");

    if let Ok(res) = handle_api_route(&r, cfg) {
        assert_eq!(res.status(), &ApiResponseStatus::Ok);
    }
}

#[test]
fn get_api_inspectors_bad_auth_generates_403() {
    let r = HttpRequest::new(
        HttpMethod::Get,
        "/api/inspectors".to_string(),
        HttpVersion::Http11,
        vec![],
        vec![],
        vec![],
        true,
    );
    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");

    if let Ok(res) = handle_api_route(&r, cfg) {
        assert_eq!(res.status(), &ApiResponseStatus::Forbidden);
    }
}

#[test]
fn get_api_inspectors_good_auth_generates_200() {
    initialize();

    let auth = HttpHeader::new("rp_auth".to_string(), "tests".to_string());
    let r = HttpRequest::new(
        HttpMethod::Get,
        "/api/inspectors/".to_string(),
        HttpVersion::Http11,
        vec![auth],
        vec![],
        vec![],
        true,
    );
    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");
    println!("{:?}", r);
    if let Ok(res) = handle_api_route(&r, cfg) {
        assert_eq!(res.status(), &ApiResponseStatus::Ok);
    }
}

#[test]
fn get_api_inspectors_good_auth_and_bad_id_generates_200_with_empty_array() {
    initialize();

    let auth = HttpHeader::new("rp_auth".to_string(), "tests".to_string());
    let r = HttpRequest::new(
        HttpMethod::Get,
        "/api/inspectors/123456".to_string(),
        HttpVersion::Http11,
        vec![auth],
        vec![],
        vec![],
        true,
    );
    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");

    if let Ok(res) = handle_api_route(&r, cfg) {
        assert_eq!(res.status(), &ApiResponseStatus::Ok);
    }
}

#[test]
fn del_api_inspectors_good_auth_no_id_generates_404() {
    initialize();

    let auth = HttpHeader::new("rp_auth".to_string(), "tests".to_string());
    let r = HttpRequest::new(
        HttpMethod::Delete,
        "/api/inspectors/".to_string(),
        HttpVersion::Http11,
        vec![auth],
        vec![],
        vec![],
        true,
    );
    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");

    if let Ok(res) = handle_api_route(&r, cfg) {
        assert_eq!(res.status(), &ApiResponseStatus::NotFound);
    }
}

#[test]
fn del_api_inspectors_good_auth_bad_id_generates_404() {
    initialize();

    let auth = HttpHeader::new("rp_auth".to_string(), "tests".to_string());
    let r = HttpRequest::new(
        HttpMethod::Delete,
        "/api/inspectors/123456".to_string(),
        HttpVersion::Http11,
        vec![auth],
        vec![],
        vec![],
        true,
    );
    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");

    if let Ok(res) = handle_api_route(&r, cfg) {
        assert_eq!(res.status(), &ApiResponseStatus::NotFound);
    }
}

#[test]
fn del_api_inspectors_good_auth_good_id_generates_200() {
    initialize();

    let auth = HttpHeader::new("rp_auth".to_string(), "tests".to_string());
    let r = HttpRequest::new(
        HttpMethod::Delete,
        "/api/inspectors/1".to_string(),
        HttpVersion::Http11,
        vec![auth],
        vec![],
        vec![],
        true,
    );
    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");

    if let Ok(res) = handle_api_route(&r, cfg) {
        assert_eq!(res.status(), &ApiResponseStatus::Ok);
    }
}

#[test]
fn new_api_inspectors_good_auth_good_json_generates_200() {
    initialize();

    let auth = HttpHeader::new("rp_auth".to_string(), "tests".to_string());
    let json = serde_json::to_string(&Inspector::default()).unwrap();

    let r = HttpRequest::new(
        HttpMethod::Post,
        "/api/inspectors/".to_string(),
        HttpVersion::Http11,
        vec![auth],
        json.as_bytes().to_vec(),
        vec![],
        true,
    );

    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");

    if let Ok(res) = handle_api_route(&r, cfg) {
        println!("{:?}", res);
        assert_eq!(res.status(), &ApiResponseStatus::Ok);
    }
}

#[test]
fn new_api_inspectors_good_auth_bad_json_generates_400() {
    initialize();

    let auth = HttpHeader::new("rp_auth".to_string(), "tests".to_string());

    let r = HttpRequest::new(
        HttpMethod::Post,
        "/api/inspectors/".to_string(),
        HttpVersion::Http11,
        vec![auth],
        "".as_bytes().to_vec(),
        vec![],
        true,
    );

    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");

    if let Ok(res) = handle_api_route(&r, cfg) {
        println!("{:?}", res);
        assert_eq!(res.status(), &ApiResponseStatus::BadRequest);
    }
}

#[test]
fn update_api_inspectors_good_auth_good_id_generates_200() {
    initialize();

    let auth = HttpHeader::new("rp_auth".to_string(), "tests".to_string());
    let inspector = Inspector {
        target: "httpbin.org".to_string(),
        ..Default::default()
    };
    let json = serde_json::to_string(&inspector).unwrap();
    let r = HttpRequest::new(
        HttpMethod::Patch,
        "/api/inspectors/2".to_string(), // id will be 2 after the "new" above
        HttpVersion::Http11,
        vec![auth],
        json.as_bytes().to_vec(),
        vec![],
        true,
    );
    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");

    if let Ok(res) = handle_api_route(&r, cfg) {
        println!("{:?}", res);
        assert_eq!(res.status(), &ApiResponseStatus::Ok);
    }
}

#[test]
fn update_api_inspectors_good_auth_bad_id_generates_400() {
    initialize();

    let auth = HttpHeader::new("rp_auth".to_string(), "tests".to_string());
    let inspector = Inspector {
        target: "httpbin.org".to_string(),
        ..Default::default()
    };
    let json = serde_json::to_string(&inspector).unwrap();
    let r = HttpRequest::new(
        HttpMethod::Patch,
        "/api/inspectors/0".to_string(), // id will be 2 after the "new" above
        HttpVersion::Http11,
        vec![auth],
        json.as_bytes().to_vec(),
        vec![],
        true,
    );
    let mut cfg = Config::default();
    cfg.set_shared_secret("tests");
    cfg.set_directory("./test_project/");

    if let Ok(res) = handle_api_route(&r, cfg) {
        println!("{:?}", res);
        assert_eq!(res.status(), &ApiResponseStatus::NotFound);
    }
}
*/