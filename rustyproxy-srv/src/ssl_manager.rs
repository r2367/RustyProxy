use openssl::{
    pkey::{PKey, PKeyRef, Private},
    x509::{X509Ref, X509},
};
use rusqlite::Connection;
use std::{collections::HashMap, error::Error, fs::File, io::Write};
use nomhttp::http_method::HttpMethod;
use std::str::FromStr;
use log::debug;

use crate::ssl::{mk_ca_cert, mk_ca_signed_cert};

pub fn get_ca_pair(conn: &Connection) -> Result<(X509, PKey<Private>), Box<dyn Error>> {
    let mut stmt = conn.prepare("SELECT cert, private FROM ca WHERE id=1")?;
    let result: Result<(Result<String, _>, Result<String, _>), _> =
        stmt.query_row([], |r| Ok((r.get(0), r.get(1))));
    let (ca_cert, ca_key_pair) = match result {
        Err(_) => {
            let (cert, key) = mk_ca_cert()?;

            let mut stmt =
                conn.prepare("INSERT INTO ca(cert, private, hostname) VALUES(?1, ?2, ?3)")?;
            let cert_pem = String::from_utf8_lossy(&cert.to_pem()?).to_string();
            let key_pem = String::from_utf8_lossy(&key.private_key_to_pem_pkcs8()?).to_string();
            stmt.execute([&cert_pem, &key_pem, "CA"])?;

            (cert, key)
        }
        Ok((cr, kr)) => {
            let cert_pem = cr?;
            let key_pem = kr?;

            let x509 = X509::from_pem(cert_pem.as_bytes())?;
            let private = PKey::private_key_from_pem(key_pem.as_bytes())?;

            (x509, private)
        }
    };

    Ok((ca_cert, ca_key_pair))
}

fn get_name_for_cert(hostname: &str) -> String {
    if !hostname.contains("127.0.0.1") {
        match hostname.chars().filter(|x|  *x == '.').count() {
            1 => hostname.to_string(),
            2.. => {
                format!("*{}", hostname.chars().skip_while(|x| *x != '.').collect::<String>())
            },
            _ => panic!("IMPOSSIBRU"),
        }
    } else {
        hostname.to_string()
    }
    
}

pub fn get_host_pair(
    conn: &Connection,
    hostname: &str,
    ca_cert: &X509Ref,
    ca_key_pair: &PKeyRef<Private>,
) -> Result<(X509, PKey<Private>), Box<dyn Error>> {
    let mut stmt = conn.prepare("SELECT cert, private FROM ca WHERE hostname=?1")?;
    let hostname = get_name_for_cert(&hostname);
    let result: Result<(Result<String, _>, Result<String, _>), _> =
        stmt.query_row([&hostname], |r| Ok((r.get(0), r.get(1))));

    let (cert, pair) = match result {
        Err(_) => {
            let (cert, key) = mk_ca_signed_cert(ca_cert, ca_key_pair, &hostname)?;

            let mut stmt =
                conn.prepare("INSERT INTO ca(cert, private, hostname) VALUES(?1, ?2, ?3)")?;
            let cert_pem = String::from_utf8_lossy(&cert.to_pem()?).to_string();
            let key_pem = String::from_utf8_lossy(&key.private_key_to_pem_pkcs8()?).to_string();
            stmt.execute([&cert_pem, &key_pem, &hostname])?;

            (cert, key)
        }
        Ok((cr, kr)) => {
            let cert_pem = cr?;
            let key_pem = kr?;

            let x509 = X509::from_pem(cert_pem.as_bytes())?;
            let private = PKey::private_key_from_pem(key_pem.as_bytes())?;

            (x509, private)
        }
    };

    Ok((cert, pair))
}


pub fn is_ssl_tls_handshake(bytes: &[u8]) -> bool {
    let data = String::from_utf8_lossy(&bytes);
    if let Ok(method) = HttpMethod::from_str(&data) {
        match method {
            HttpMethod::Other(data) => {
                if &bytes[..3] == [22, 3, 1] {
                    true
                } else {
                    debug!("Got unknown method: {}", data);
                    false
                }
            },
            _ => false,
        }
    } else {
        true
    }
}

pub fn save_certinfo_to_directory(d: &str, ca_cert: &X509Ref, ca_key_pair: &PKeyRef<Private>) -> Result< (), Box<dyn Error>> {
    if !std::path::Path::new(&d).exists() {
        std::fs::create_dir(&d)?;
    }
    let mut f = File::create(format!("{}/certinfo.json", d))?;
    let mut h = HashMap::new();
    h.insert("ca_cert", String::from_utf8_lossy(&ca_cert.to_pem()?).to_string());
    h.insert("ca_key_pair", String::from_utf8_lossy(&ca_key_pair.private_key_to_pem_pkcs8()?).to_string());
    let json = serde_json::to_string(&h)?;
    f.write_all(json.as_bytes())?;
    Ok(())
}

pub fn load_certinfo_from_directory(d: &str) -> Result< (X509, PKey<Private>), Box<dyn Error>> {
    let f = File::open(format!("{}/certinfo.json", d))?;
    let h: HashMap<String, String> = serde_json::from_reader(f)?;
    let x509 = X509::from_pem(&h.get("ca_cert").unwrap().as_bytes())?;
    let private = PKey::private_key_from_pem(&h.get("ca_key_pair").unwrap().as_bytes())?;
    Ok((x509, private))
}