# What is RustyProxy?

Rustyproxy is a tool to help pentesters do their job when pentesting web application. It is a replacement for tools like burp or zap, because they are in java and they quickly eat up 16Go ram during a normal pentest.

All the basic features are working (History, Repeater, Intruder and Decoder), but many are missing. 
I plan to add plugin support and stuff like that. There is support for two pentester working with shared history and repeater syncro, just make the server run on 0.0.0.0 (with a strong secret, of course).

Fair warning: This program is in alpha, you might encounter bugs. The intruder might be a little too harsh on websites and can cause a DoS of your own connection, DNS server, or your target 8D.

# Installation

## Using Nix

### GUI:
```sh
nix run https://gitlab.com/r2367/RustyProxy/-/archive/main/RustyProxy-main.tar.gz#rustyproxy
```
### Proxy server:
```sh
nix run https://gitlab.com/r2367/RustyProxy/-/archive/main/RustyProxy-main.tar.gz#rustyproxy-srv
```

## Without Nix
1/ install rust

Follow instructions at https://rustup.rs/
(its basically 1 command)

2/ install rustyproxy
```sh
cargo install rustyproxy rustyproxy-srv
```

if you have compilation errors, try installing pkgconfig libssl-dev build-essentials librust-glib-sys-dev libatk1.0-dev libgtk3-dev

# Getting started

Say that you want to attack victim.com

1/ start the server

```sh
rustyproxy-srv -d ./project_directory -s mysupersecret --scope victim.com
```

2/ start the client

`rustyproxy`

You'll land on a configuration window, configure all the things like you did from the command line and it'll connect
You can then start a browser (by default, chromium) via the Windows menu on the menu bar. From the same bar you can download the certificate from the server and then configure your brwoser with the CA. And you're good to go !
