# Rustyproxy

In an attempt to build an alternative to burp and zap, i made this small app.

The point is to take requests from a database in order to inspect/replay/bruteforce those requests.

If you want to contribute, mp me on [Matrix](https://matrix.to/#/@vaelio:matarch.fr) !

## Getting started

### Running separately Server and GUI
First, run the server:

```bash
git clone https://gitlab.com/r2367/rustyproxy-srv
cd rustyproxy-srv
./run.sh -d /tmp/RPTProject --secret omegalul --scope my.client.com
```

You can also run the server with docker, check https://gitlab.com/r2367/rustyproxy-srv for more info.

Then, run this GUI:

```bash
git clone https://github.com/vaelio/rustyproxy-egui
cd rustyproxy-egui
./run.sh
```
The GUI should take up to a few minutes to compile but overall it should be fine. 
I plan to push it to crates.io someday

## Screenshots

![dark_theme](https://user-images.githubusercontent.com/6543163/224374146-546ba9e9-f916-4633-816e-757761cbc4ee.png)
![light_theme](https://user-images.githubusercontent.com/6543163/224374176-c3f41df6-934b-48a9-b8bb-cdc14677e954.png)

This program is licensed under GPL-3.0
