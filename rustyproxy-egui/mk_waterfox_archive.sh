#!/bin/bash

echo "[i] Compressing waterfox"
tar cvf - ./waterfox | zstd -19 -o ./waterfox.tar.zstd
echo "[i] Done. Compressing waterfox profile"
tar cvf - ./profile | zstd -19 -o ./waterfox_profile.tar.zstd
