mod backend;
mod mods;
use crate::{proxy_ui, websocket_ui};
use backend::apiutils;
use backend::dbutils;
use backend::windows::{Window, Wtype};
use egui::ViewportCommand;
use hex::{decode as hexdecode, encode as hexencode};
use memory_stats::memory_stats;
use mods::browser_settings::BrowserSettings;
use mods::decoder::Decoder;
use mods::history::{History, Selected, View};
use mods::inspector::Inspector;
use mods::interact_sh::Interact;
use mods::notes::NoteViewer;
use mods::openapi::OpenApiWindow;
use poll_promise::Promise;
use reqwest::header::HeaderMap;
use std::io::Write;
use std::ops::Range;
use std::process::Command;
use thousands::Separable;
use urlencoding::{decode as urldecode, encode as urlencode};

use self::mods::history;

/// We derive Deserialize/Serialize so we can persist app state on shutdown.
#[derive(serde::Deserialize, serde::Serialize)]
#[serde(default)] // if we add new fields, give them default values when deserializing old state
pub struct TemplateApp {
    // Example stuff:
    #[serde(skip)]
    menu: String,
    // picked_path
    #[serde(skip)]
    picked_path: Option<String>,
    // Components
    #[serde(skip)]
    windows: Vec<Window>,
    #[serde(skip)]
    proxy: Window,
    #[serde(skip)]
    api_addr: Option<String>,
    #[serde(skip)]
    api_port: Option<usize>,
    #[serde(skip)]
    api_secret: Option<String>,
    api_addr_input: String,
    api_port_input: String,
    api_secret_input: String,
    proxy_addr_input: String,
    proxy_port_input: String,
    proxy_addr: Option<String>,
    proxy_port: Option<usize>,
    #[serde(skip)]
    history: History,
    #[serde(skip)]
    zoom_level: String,
    zoom_level_parsed: f32,
    #[serde(skip)]
    decoder_panel: bool,
    decoder: Decoder,
    #[serde(skip)]
    is_connected: bool,
    #[serde(skip)]
    cert_save_path: Option<String>,
    #[serde(skip)]
    promise_cert_download: Option<Promise<Result<String, reqwest::Error>>>,
    browser_settings: BrowserSettings,
    enable_paging: bool,
}
impl Default for TemplateApp {
    fn default() -> Self {
        //windows: vec![Window { name: "Proxy".to_string(), wtype: Wtype::Proxy, ..Default::default()}],
        Self {
            menu: "History".to_owned(),
            picked_path: None,
            windows: vec![],
            api_addr: None,
            api_port: None,
            api_secret: None,
            proxy_addr: None,
            proxy_port: None,
            proxy_addr_input: String::new(),
            proxy_port_input: String::new(),
            api_addr_input: String::new(),
            api_port_input: String::new(),
            api_secret_input: String::new(),
            history: History::default(),
            zoom_level: String::from("1.0"),
            zoom_level_parsed: 1.0,
            decoder_panel: false,
            decoder: Decoder::default(),
            promise_cert_download: None,
            cert_save_path: None,
            is_connected: false,
            enable_paging: false,
            browser_settings: BrowserSettings::default(),
            proxy: Window {
                name: "Proxy".to_string(),
                wtype: Wtype::Proxy,
                ..Default::default()
            },
        }
    }
}
impl TemplateApp {
    /// Called once before the first frame.
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        // This is also where you can customized the look at feel of egui using
        // `cc.egui_ctx.set_visuals` and `cc.egui_ctx.set_fonts`.
        let mut vis = egui::Visuals::light();

        vis.button_frame = true;
        vis.indent_has_left_vline = true;
        vis.striped = true;
        vis.slider_trailing_fill = true;
        vis.collapsing_header_frame = true;

        cc.egui_ctx.set_visuals(vis);
        // Load previous app state (if any).
        // Note that you must enable the `persistence` feature for this to work.
        if let Some(storage) = cc.storage {
            return eframe::get_value(storage, eframe::APP_KEY).unwrap_or_default();
        }
        Default::default()
    }
    pub fn is_connected(&self) -> bool {
        self.is_connected
    }
    fn url(&self) -> String {
        match (self.api_addr.as_ref(), self.api_port.as_ref()) {
            (Some(addr), Some(port)) => format!("{}:{}", addr, port),
            (Some(addr), None) => format!("{}:8443", addr),
            (None, Some(port)) => format!("127.0.0.1:{}", port),
            (None, None) => "127.0.0.1:8443".to_string(),
        }
    }
    fn api_secret(&self) -> String {
        match self.api_secret.as_ref() {
            Some(secret) => secret.to_string(),
            _ => "none".to_string(),
        }
    }

    #[cfg(target_os = "linux")]
    fn start_browser(&self) {
        let cmd = self
            .browser_settings
            .command(&self.proxy_addr_input, &self.proxy_port_input);

        let _ = Command::new("sh")
            .arg("-c")
            .arg(cmd)
            .spawn()
            .unwrap_or_else(|_| {
                panic!(
                    "{} failed to start",
                    self.browser_settings
                        .binary_path()
                        .as_ref()
                        .unwrap_or(&"chromium".to_string())
                )
            });
    }

    #[cfg(target_os = "windows")]
    fn start_browser(&self) {
        let cmd = self
            .browser_settings
            .command(&self.proxy_addr_input, &self.proxy_port_input);

        let _ = Command::new("cmd.exe")
            .arg("/c")
            .arg(cmd)
            .spawn()
            .unwrap_or_else(|_| {
                panic!(
                    "{} failed to start",
                    self.browser_settings
                        .binary_path()
                        .as_ref()
                        .unwrap_or(&"chromium".to_string())
                )
            });
    }
}
impl eframe::App for TemplateApp {
    /// Called by the frame work to save state before shutdown.
    fn save(&mut self, storage: &mut dyn eframe::Storage) {
        eframe::set_value(storage, eframe::APP_KEY, self);
    }
    /// Called each time the UI needs repainting, which may be many times per second.
    /// Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        // Examples of how to create different panels and windows.
        // Pick whichever suits you.
        // Tip: a good default choice is to just keep the `CentralPanel`.
        // For inspiration and more examples, go to https://emilk.github.io/egui
        ctx.request_repaint_after(std::time::Duration::from_secs_f32(2.0));
        if ctx.pixels_per_point() != self.zoom_level_parsed {
            ctx.set_pixels_per_point(self.zoom_level_parsed);
        }
        if let Some(p) = &self.promise_cert_download {
            if let Some(Ok(res)) = p.ready() {
                let path = self.cert_save_path.as_ref().unwrap();
                let mut file = std::fs::OpenOptions::new()
                    .write(true)
                    .create(true)
                    .truncate(true)
                    .open(path)
                    .unwrap();
                file.write_all(res.as_bytes()).unwrap();
                self.promise_cert_download = None;
                self.cert_save_path = None;
            }
        }
        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            // The top panel is often a good place for a menu bar:
            egui::menu::bar(ui, |ui| {
                egui::widgets::global_dark_light_mode_switch(ui);
                ui.separator();
                ui.menu_button("File", |ui| {
                    if ui.button("New").clicked() {
                        self.windows = vec![];
                        self.proxy.is_active = true;
                        self.picked_path = None;
                        self.api_addr = None;
                        self.api_port = None;
                        self.api_secret = None;
                        self.is_connected = false;
                        ui.close_menu();
                    }
                    if ui.button("Quit").clicked() {
                        ui.close_menu();
                        ctx.send_viewport_cmd(ViewportCommand::Close);
                    }
                });
                ui.separator();
                ui.menu_button("Windows", |ui| {
                    if self.is_connected() && ui.button("Open history window").clicked() {
                        if let Some(w) = self.windows.iter_mut().find(|x| x.name == "history") {
                            w.is_active = true;
                        } else {
                            self.windows.push(Window {
                                name: "History".to_string(),
                                wtype: Wtype::History,
                                api_addr: self.api_addr.clone(),
                                api_port: self.api_port,
                                api_secret: self.api_secret.clone(),
                                proxy_addr: self.proxy_addr.clone(),
                                proxy_port: self.proxy_port,
                                clicked: false,
                                ..Default::default()
                            });
                        }
                        ui.close_menu();
                    }
                    if self.is_connected() && ui.button("New interactsh").clicked() {
                        let int = Interact::new();
                        self.windows.push(Window {
                            name: format!("Interactsh {}", int.correlation_id()),
                            wtype: Wtype::Interactsh(int),
                            api_addr: self.api_addr.clone(),
                            api_port: self.api_port,
                            api_secret: self.api_secret.clone(),
                            proxy_addr: self.proxy_addr.clone(),
                            proxy_port: self.proxy_port,
                            clicked: false,
                            ..Default::default()
                        });
                        ui.close_menu();
                    }
                    if self.is_connected() && ui.button("OpenAPI").clicked() {
                        if let Some(path) = rfd::FileDialog::new().pick_file() {
                            let oaw = OpenApiWindow::new(path.display());
                            self.windows.push(Window {
                                name: format!("OpenApi {}", path.display()),
                                wtype: Wtype::OpenApi(oaw),
                                api_addr: self.api_addr.clone(),
                                api_port: self.api_port,
                                api_secret: self.api_secret.clone(),
                                proxy_addr: self.proxy_addr.clone(),
                                proxy_port: self.proxy_port,
                                clicked: false,
                                ..Default::default()
                            });
                        }
                        ui.close_menu();
                    }
                    if self.is_connected() && ui.button("Open browser").clicked() {
                        self.start_browser();
                        ui.close_menu();
                    }
                });
                ui.separator();
                ui.menu_button("Settings", |ui| {
                    if ui.button("EGUI Settings UI").clicked() {
                        self.windows.push(Window {
                            name: "Settings UI".to_string(),
                            wtype: Wtype::Settings,
                            api_addr: self.api_addr.clone(),
                            api_port: self.api_port,
                            api_secret: self.api_secret.clone(),
                            proxy_addr: self.proxy_addr.clone(),
                            proxy_port: self.proxy_port,
                            clicked: false,
                            ..Default::default()
                        });
                        ui.close_menu();
                    }
                    if ui.button("Browser settings").clicked() {
                        self.browser_settings.set_open(true);
                        ui.close_menu();
                    }
                    if ui.button("Proxy Settings").clicked() {
                        self.windows = vec![];
                        self.proxy.is_active = true;
                        self.picked_path = None;
                        self.api_addr = None;
                        self.api_port = None;
                        self.api_secret = None;
                        self.proxy_addr = None;
                        self.proxy_port = None;
                        self.is_connected = false;
                        ui.close_menu();
                    }

                    if ui.button("EGUI Memory UI").clicked() {
                        self.windows.push(Window {
                            name: "Memory UI".to_string(),
                            wtype: Wtype::Memory,
                            api_addr: self.api_addr.clone(),
                            api_port: self.api_port,
                            api_secret: self.api_secret.clone(),
                            proxy_addr: self.proxy_addr.clone(),
                            proxy_port: self.proxy_port,
                            clicked: false,
                            ..Default::default()
                        });
                        ui.close_menu();
                    }
                });
                ui.separator();
                ui.label("Zoom: ");
                let resp = ui.text_edit_singleline(&mut self.zoom_level);
                if resp.lost_focus() && ui.input(|i| i.key_pressed(egui::Key::Enter)) {
                    if let Ok(value) = self.zoom_level.parse::<f32>() {
                        self.zoom_level_parsed = value;
                    }
                }
                ui.separator();
                let txt = if self.is_connected() {
                    "Connected"
                } else {
                    "Disconnected"
                };
                let _ = ui.selectable_label(self.is_connected(), txt);
                if self.is_connected() {
                    /* Connected */
                    ui.separator();
                    if ui
                        .selectable_label(
                            {
                                if let Some(w) = self
                                    .windows
                                    .iter_mut()
                                    .find(|x| x.name.to_lowercase() == "history")
                                {
                                    /* Found a history window */
                                    w.is_active
                                } else {
                                    false
                                }
                            },
                            "☰ History window",
                        )
                        .clicked()
                    {
                        if let Some(w) = self
                            .windows
                            .iter_mut()
                            .find(|x| x.name.to_lowercase() == "history")
                        {
                            w.is_active = !w.is_active;
                        }
                    }
                    ui.separator();
                    if ui
                        .selectable_label(self.decoder_panel, "☰ Encoder/Decoder Panel")
                        .clicked()
                    {
                        self.decoder_panel = !self.decoder_panel;
                    }
                    ui.separator();
                    if ui.button("⬇ Download Cert").clicked() {
                        if let Some(path) = rfd::FileDialog::new().save_file() {
                            self.cert_save_path = Some(format!("{}", path.display()));
                            let url = format!(
                                "{}:{}",
                                self.api_addr.clone().unwrap(),
                                self.api_port.unwrap()
                            );
                            let secret = self.api_secret.clone().unwrap();
                            self.promise_cert_download.get_or_insert_with(|| {
                                Promise::spawn_thread("dl_cert", move || {
                                    apiutils::download_cert_from_api(&url, &secret)
                                })
                            });
                        }
                    }
                }
                if let Some(picked_path) = &self.picked_path {
                    ui.with_layout(egui::Layout::top_down(egui::Align::RIGHT), |ui| {
                        ui.horizontal(|ui| {
                            ui.monospace(picked_path);
                            ui.label("🗀 Project Path: ");
                            ui.separator();
                        });
                    });
                }
            });
        });

        egui::TopBottomPanel::bottom("bottom_panel").show(ctx, |ui| {
            ui.horizontal(|ui| {
                if let Some(usage) = memory_stats() {
                    ui.label(format!(
                        "virt: {}, phys: {}",
                        usage.virtual_mem.separate_with_spaces(),
                        usage.physical_mem.separate_with_spaces()
                    ));
                }
                ui.separator();
                ui.label("This program is yet in alpha. Feel free to contribute on");
                ui.hyperlink_to("Gitlab", "https://gitlab.com/r2367/RustyProxy");
            });
        });
        if self.decoder_panel && self.is_connected() {
            egui::SidePanel::left("decoder_left_panel").show(ctx, |ui| {
                self.decoder.display(ui);
            });
        }
        egui::CentralPanel::default().show(ctx, |ui| {
            // The central panel the region left after adding TopPanel's and SidePanel's
            /*
            show basic project window
            */
            let mut windows_to_add = vec![];
            let mut windows_to_remove = vec![];
            if self.proxy.is_active {
                egui::Window::new("Connect to proxy").show(ctx, |ui| {
                    proxy_ui!(
                        ui,
                        self.proxy,
                        &mut self.api_addr_input,
                        &mut self.api_port_input,
                        &mut self.api_secret_input,
                        &mut self.proxy_addr_input,
                        &mut self.proxy_port_input,
                        &mut self.enable_paging
                    );
                    self.api_addr = self.proxy.api_addr.clone();
                    self.api_port = self.proxy.api_port;
                    self.api_secret = self.proxy.api_secret.clone();
                    self.proxy_addr = self.proxy.proxy_addr.clone();
                    self.proxy_port = self.proxy.proxy_port;
                    if self.proxy.clicked {
                        self.proxy.is_active = false;
                        windows_to_add.push(Window {
                            name: "History".to_string(),
                            wtype: Wtype::History,
                            api_addr: self.api_addr.clone(),
                            api_port: self.api_port,
                            api_secret: self.api_secret.clone(),
                            proxy_addr: self.proxy_addr.clone(),
                            proxy_port: self.proxy_port,
                            clicked: false,
                            ..Default::default()
                        });

                        let api_url = format!(
                            "{}:{}",
                            self.api_addr.as_ref().unwrap(),
                            self.api_port.as_ref().unwrap()
                        );
                        let api_addr = self.api_addr.as_ref().unwrap();
                        let api_port = self.api_port.as_ref().unwrap();
                        let api_secret = self.api_secret.as_ref().unwrap();
                        let proxy_addr = self.proxy_addr.as_ref().unwrap();
                        let proxy_port = self.proxy_port.as_ref().unwrap();
                        self.history = History::new(
                            &api_url,
                            &api_addr,
                            *api_port,
                            &proxy_addr,
                            *proxy_port,
                            &api_secret,
                        );
                        self.proxy.clicked = false;
                        self.is_connected = !self.history.had_errors
                    }
                });
            } else {
                if self.browser_settings.is_opened() {
                    self.browser_settings.display(ui);
                }
                let url = self.url();
                let secret = self.api_secret();
                for w in &mut self.windows {
                    if w.is_active {
                        match &mut w.wtype {
                            Wtype::Settings => {
                                egui::Window::new(w.name.to_string()).title_bar(false).show(
                                    ctx,
                                    |ui| {
                                        egui::menu::bar(ui, |ui| {
                                            ui.with_layout(
                                                egui::Layout::top_down(egui::Align::RIGHT),
                                                |ui| {
                                                    ui.horizontal(|ui| {
                                                        if ui.button("x").clicked() {
                                                            w.is_active = false;
                                                            ui.ctx().request_repaint();
                                                        }
                                                    });
                                                },
                                            );
                                        });
                                        ctx.settings_ui(ui);
                                    },
                                );
                            }
                            Wtype::OpenApi(openapiwindow) => {
                                egui::Window::new(w.name.to_string()).title_bar(false).show(
                                    ctx,
                                    |ui| {
                                        egui::menu::bar(ui, |ui| {
                                            // eventual menu
                                            ui.with_layout(
                                                egui::Layout::top_down(egui::Align::RIGHT),
                                                |ui| {
                                                    ui.horizontal(|ui| {
                                                        if ui.button("x").clicked() {
                                                            w.is_active = false;
                                                            ui.ctx().request_repaint();
                                                        }
                                                        ui.separator();
                                                        let bt = if openapiwindow.is_minimized() {
                                                            "+"
                                                        } else {
                                                            "-"
                                                        };
                                                        if ui.button(bt).clicked() {
                                                            openapiwindow.minimized(
                                                                !openapiwindow.is_minimized(),
                                                            );
                                                            ui.ctx().request_repaint();
                                                        }
                                                        ui.separator();
                                                        ui.with_layout(
                                                            egui::Layout::top_down(
                                                                egui::Align::Center,
                                                            ),
                                                            |ui| {
                                                                ui.monospace(format!(
                                                                    "OpenApi: #{}",
                                                                    openapiwindow.path()
                                                                ));
                                                            },
                                                        );
                                                    });
                                                },
                                            );
                                        });
                                        ui.separator();
                                        openapiwindow.display(ui);
                                    },
                                );
                            }
                            Wtype::Memory => {
                                egui::Window::new(w.name.to_string()).title_bar(false).show(
                                    ctx,
                                    |ui| {
                                        egui::menu::bar(ui, |ui| {
                                            ui.with_layout(
                                                egui::Layout::top_down(egui::Align::RIGHT),
                                                |ui| {
                                                    ui.horizontal(|ui| {
                                                        if ui.button("x").clicked() {
                                                            w.is_active = false;
                                                            ui.ctx().request_repaint();
                                                        }
                                                    });
                                                },
                                            );
                                        });
                                        ctx.memory_ui(ui);
                                    },
                                );
                            }
                            Wtype::History => {
                                ui.group(|ui| {
                                    self.history.display(ui, w);
                                    ui.separator();
                                    if self.history.active_view == View::History {
                                        self.history.preview_ui(ui);
                                        if ui.ui_contains_pointer() {
                                            if ui.input(|i| i.key_pressed(egui::Key::ArrowDown)) {
                                                if self.history.preview_id
                                                    < self.history.items_per_page - 1
                                                {
                                                    self.history.preview_id += 1;
                                                }
                                            }
                                            if ui.input(|i| i.key_pressed(egui::Key::ArrowUp)) {
                                                if self.history.preview_id != 0 {
                                                    self.history.preview_id -= 1;
                                                }
                                            }
                                            if ui.input(|i| i.key_pressed(egui::Key::ArrowRight)) {
                                                match self.history.filter.is_some() {
                                                    true => {
                                                        self.history.last_seen_idx_filtered +=
                                                            self.history.items_per_page
                                                    }
                                                    false => {
                                                        self.history.last_seen_idx +=
                                                            self.history.items_per_page
                                                    }
                                                }
                                            }
                                            if ui.input(|i| i.key_pressed(egui::Key::ArrowLeft)) {
                                                match (
                                                    self.history.filter.is_some(),
                                                    self.history.last_seen_idx_filtered as i32
                                                        - self.history.items_per_page as i32,
                                                    self.history.last_seen_idx as i32
                                                        - self.history.items_per_page as i32,
                                                ) {
                                                    (true, 0.., _) => {
                                                        self.history.last_seen_idx_filtered -=
                                                            self.history.items_per_page
                                                    } // $last_seen_idx_filtered >= $items_per_page
                                                    (true, ..=-1, _) => {
                                                        self.history.last_seen_idx_filtered = 0
                                                    } // $last_seen_idx_filtered < $items_per_page
                                                    (false, _, 0..) => {
                                                        self.history.last_seen_idx -=
                                                            self.history.items_per_page
                                                    } // $last_seen_idx >= $items_per_page
                                                    (false, _, ..=-1) => {
                                                        self.history.last_seen_idx = 0
                                                    } // $last_seen_idx < $items_per_page
                                                }
                                            }
                                            if ui.input_mut(|i| {
                                                i.consume_key(
                                                    egui::Modifiers {
                                                        command: true,
                                                        shift: true,
                                                        ..Default::default()
                                                    },
                                                    egui::Key::R,
                                                )
                                            }) {
                                                if let Some(h) =
                                                    self.history.get_currently_selected()
                                                {
                                                    windows_to_add.push(Window {
                                                        name: format!("Inspecting #{}", h.id()),
                                                        wtype: Wtype::Inspector(
                                                            Inspector::from_histline(
                                                                &h,
                                                                &self.proxy_addr.clone(),
                                                                &self.proxy_port.clone(),
                                                            ),
                                                        ),
                                                        api_addr: self.api_addr.clone(),
                                                        api_port: self.api_port,
                                                        api_secret: self.api_secret.clone(),
                                                        proxy_addr: self.proxy_addr.clone(),
                                                        proxy_port: self.proxy_port,
                                                        clicked: false,
                                                        ..Default::default()
                                                    });
                                                }
                                            }
                                            if ui.input_mut(|i| {
                                                i.consume_key(
                                                    egui::Modifiers {
                                                        command: true,
                                                        ..Default::default()
                                                    },
                                                    egui::Key::R,
                                                )
                                            }) {
                                                if let Some(h) =
                                                    self.history.get_currently_selected()
                                                {
                                                    self.history.inspectors.add_inspector(
                                                        Inspector::from_histline(
                                                            h,
                                                            &self.proxy_addr,
                                                            &self.proxy_port,
                                                        ),
                                                    );
                                                    self.history.active_view =
                                                        history::View::Inspectors;
                                                }
                                            }
                                        }
                                    }
                                });

                                if w.clicked {
                                    match self.history.selected() {
                                        Some(Selected::Histline(h)) => {
                                            windows_to_add.push(Window {
                                                name: format!("Inspecting #{}", h.id()),
                                                wtype: Wtype::Inspector(Inspector::from_histline(
                                                    &h,
                                                    &self.proxy_addr,
                                                    &self.proxy_port,
                                                )),
                                                api_addr: self.api_addr.clone(),
                                                api_port: self.api_port,
                                                api_secret: self.api_secret.clone(),
                                                proxy_addr: self.proxy_addr.clone(),
                                                proxy_port: self.proxy_port,
                                                clicked: false,
                                                ..Default::default()
                                            });
                                        }
                                        Some(Selected::Inspector(i)) => {
                                            windows_to_add.push(Window {
                                                name: format!("Inspector Remote #{}", i.id),
                                                wtype: Wtype::Inspector(Inspector::from_inspector(
                                                    &i,
                                                    &self.proxy_addr.clone(),
                                                    &self.proxy_port.clone(),
                                                )),
                                                api_addr: self.api_addr.clone(),
                                                api_port: self.api_port,
                                                api_secret: self.api_secret.clone(),
                                                proxy_addr: self.proxy_addr.clone(),
                                                proxy_port: self.proxy_port,
                                                clicked: false,
                                                ..Default::default()
                                            });
                                        }
                                        Some(Selected::Websockets(ws)) => {
                                            windows_to_add.push(Window {
                                                name: format!("Websocket viewer {}", ws.stream_rep),
                                                wtype: Wtype::WebSocketViewer(ws.clone()),
                                                api_addr: self.api_addr.clone(),
                                                api_port: self.api_port,
                                                api_secret: self.api_secret.clone(),
                                                proxy_addr: self.proxy_addr.clone(),
                                                proxy_port: self.proxy_port,
                                                clicked: false,
                                                ..Default::default()
                                            })
                                        }
                                        Some(Selected::Note(note, is_new)) => {
                                            let nv = NoteViewer::from_note(&note, is_new);
                                            windows_to_add.push(Window {
                                                name: format!("Note viewer {}", nv.note().id()),
                                                wtype: Wtype::NoteViewer(nv),
                                                api_addr: self.api_addr.clone(),
                                                api_port: self.api_port,
                                                api_secret: self.api_secret.clone(),
                                                proxy_addr: self.proxy_addr.clone(),
                                                proxy_port: self.proxy_port,
                                                clicked: false,
                                                ..Default::default()
                                            })
                                        }
                                        _ => {}
                                    }
                                    w.clicked = false;
                                    self.history.selected = None;
                                }
                            }
                            Wtype::NoteViewer(note) => {
                                egui::Window::new(&w.name)
                                    .title_bar(false)
                                    .min_width(980.0)
                                    .show(ctx, |ui| {
                                        note.display(ui, &url, &secret);
                                    });
                                if !note.is_active() {
                                    windows_to_remove.push(w.name.to_string());
                                }
                            }
                            Wtype::WebSocketViewer(ws) => {
                                egui::Window::new(format!("WebSocket Viewer {}", ws.stream_rep))
                                    .title_bar(false)
                                    .min_width(980.0)
                                    .show(ctx, |ui| {
                                        websocket_ui!(ui, w, ws);
                                    });
                            }

                            Wtype::Interactsh(int) => {
                                egui::Window::new(format!("Interactsh {}", int.correlation_id()))
                                    .title_bar(false)
                                    .min_width(980.0)
                                    .show(ctx, |ui| {
                                        int.display(ui);
                                    });
                                if !int.is_active() {
                                    windows_to_remove.push(w.name.to_string());
                                }
                            }
                            Wtype::Inspector(i) => {
                                egui::Window::new(format!("Child {}", w.name))
                                    .title_bar(false)
                                    .min_width(980.0)
                                    .show(ctx, |ui| {
                                        let clicked = i.display(
                                            ui,
                                            w.api_addr.clone(),
                                            w.api_port,
                                            w.api_secret.clone(),
                                        );
                                        //inspector_ui!(ui, w, i);
                                        if clicked {
                                            let (idx, version, status, headers, text, payload) =
                                                i.selected.as_ref().unwrap();
                                            let response = if payload.is_empty() {
                                                text.to_string()
                                            } else {
                                                format!(
                                                    "{} {}\r\n{}\r\n{}",
                                                    version, status, headers, text
                                                )
                                            };
                                            let request =
                                                i.bf_request.replace("$[PAYLOAD]$", payload);
                                            let ins = Inspector {
                                                id: *idx,
                                                source: "RustyProxy".to_string(),
                                                request: request.to_string(),
                                                response: response.to_string(),
                                                modified_request: request.replace('\r', "\\r\\n"),
                                                new_response: response,
                                                ssl: i.ssl,
                                                target: i.target.to_string(),
                                                is_active: true,
                                                bf_request: request
                                                    .to_string()
                                                    .replace('\r', "\\r\\n"),
                                                ..Default::default()
                                            };
                                            windows_to_add.push(Window {
                                                name: format!("Inspecting #{}", ins.id),
                                                wtype: Wtype::Inspector(ins),
                                                api_addr: self.api_addr.clone(),
                                                api_port: self.api_port,
                                                api_secret: self.api_secret.clone(),
                                                proxy_addr: self.proxy_addr.clone(),
                                                proxy_port: self.proxy_port,
                                                clicked: false,
                                                ..Default::default()
                                            });
                                        }
                                        if !i.is_active {
                                            windows_to_remove.push(w.name.to_string())
                                        }
                                    });
                            }
                            Wtype::Proxy | Wtype::BrSettings(_) => { /* these should never show */ }
                        };
                    }
                }
            }
            self.windows.append(&mut windows_to_add);
            self.windows
                .retain(|w| !windows_to_remove.contains(&w.name));
            /* check wether or not there's an inspector open or not */
            egui::warn_if_debug_build(ui);
        });
    }
}
