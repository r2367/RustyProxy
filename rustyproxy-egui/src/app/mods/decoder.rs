use crate::app::mods::inspector::code_view_ui;
use crate::app::{hexdecode, hexencode};
use crate::app::{urldecode, urlencode};
use base64::{engine::general_purpose, Engine as _};

#[derive(serde::Deserialize, serde::Serialize, Debug)]
pub enum Encoding {
    Base64,
    Hex,
    Url,
}

impl Default for Encoding {
    fn default() -> Self {
        Self::Base64
    }
}

#[derive(serde::Deserialize, serde::Serialize, Debug)]
pub enum Mode {
    Decoder,
    Encoder,
}

impl Default for Mode {
    fn default() -> Self {
        Self::Decoder
    }
}

#[derive(serde::Deserialize, serde::Serialize)]
#[serde(default)]
#[derive(Default)]
pub struct Decoder {
    input: String,
    active_mode: Mode,
    active_encoding: Encoding,
}

impl Decoder {
    pub fn input_mut(&mut self) -> &mut String {
        &mut self.input
    }

    pub fn active_mode(&self) -> &Mode {
        &self.active_mode
    }

    pub fn set_active_mode(&mut self, active_mode: Mode) {
        self.active_mode = active_mode;
    }

    pub fn active_encoding(&self) -> &Encoding {
        &self.active_encoding
    }

    pub fn set_active_encoding(&mut self, active_encoding: Encoding) {
        self.active_encoding = active_encoding;
    }

    pub fn decode_base64(&self) -> String {
        if let Ok(decoded) = general_purpose::STANDARD.decode(&self.input) {
            String::from_utf8_lossy(&decoded).to_string()
        } else {
            "Could not b64decode".to_string()
        }
    }

    pub fn encode_base64(&self) -> String {
        general_purpose::STANDARD.encode(&self.input)
    }

    pub fn decode_hex(&self) -> String {
        if let Ok(decoded) = hexdecode(&self.input) {
            String::from_utf8_lossy(&decoded).to_string()
        } else {
            "Could not hexdecode".to_string()
        }
    }

    pub fn encode_hex(&self) -> String {
        hexencode(&self.input).to_string()
    }

    pub fn decode_url(&self) -> String {
        if let Ok(decoded) = urldecode(&self.input) {
            decoded.to_string()
        } else {
            "Could not urldecode".to_string()
        }
    }

    pub fn encode_url(&self) -> String {
        urlencode(&self.input).to_string()
    }

    pub fn display(&mut self, ui: &mut egui::Ui) {
        ui.vertical(|ui| {
            ui.label("Decode/Encode panel");
            ui.separator();
            ui.text_edit_multiline(self.input_mut());
            ui.separator();
            ui.horizontal(|ui| {
                ui.menu_button(format!("⏷ {:?}", self.active_mode()), |ui| {
                    if ui.button("Decode").clicked() {
                        self.set_active_mode(Mode::Decoder);
                        ui.close_menu();
                    }
                    if ui.button("Encode").clicked() {
                        self.set_active_mode(Mode::Encoder);
                        ui.close_menu();
                    }
                });
                ui.menu_button(format!("⏷ {:?}", self.active_encoding()), |ui| {
                    if ui.button("Base64").clicked() {
                        self.set_active_encoding(Encoding::Base64);
                        ui.close_menu();
                    }
                    if ui.button("Hex").clicked() {
                        self.set_active_encoding(Encoding::Hex);
                        ui.close_menu();
                    }
                    if ui.button("URL").clicked() {
                        self.set_active_encoding(Encoding::Url);
                        ui.close_menu();
                    }
                });
            });
            ui.separator();
            let output = match (self.active_encoding(), self.active_mode()) {
                (Encoding::Base64, Mode::Decoder) => self.decode_base64(),
                (Encoding::Base64, Mode::Encoder) => self.encode_base64(),
                (Encoding::Hex, Mode::Decoder) => self.decode_hex(),
                (Encoding::Hex, Mode::Encoder) => self.encode_hex(),
                (Encoding::Url, Mode::Decoder) => self.decode_url(),
                (Encoding::Url, Mode::Encoder) => self.encode_url(),
            };
            code_view_ui(ui, &output);
            ui.separator();
        });
    }
}
