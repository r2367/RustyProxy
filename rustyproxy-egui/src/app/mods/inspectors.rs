use super::inspector::Inspector;

#[derive(Debug, Default)]
pub struct Inspectors {
    items: Vec<Inspector>,
    selected: usize,
    api_addr: Option<String>,
    api_port: Option<usize>,
    api_secret: Option<String>,
}

impl Inspectors {
    pub fn display(&mut self, ui: &mut egui::Ui) {
        egui::menu::bar(ui, |ui| {
            for idx in 0..self.items.len() {
                if ui
                    .selectable_label(idx == self.selected, idx.to_string())
                    .clicked()
                {
                    self.selected = idx;
                }
                ui.separator();
            }
        });
        ui.separator();
        let api_addr = self.api_addr.clone();
        let api_port = self.api_port.clone();
        let api_secret = self.api_secret.clone();
        if let Some(ins) = self.items.get_mut(self.selected) {
            ins.display(ui, api_addr, api_port, api_secret);

            if let Some((idx, version, status, resp_headers, resp_body, payload)) = &ins.selected {
                let request = &ins.bf_request.replace("$[PAYLOAD]$", payload);
                let response = format!("{} {}\r\n{}\r\n{}", version, status, resp_headers, resp_body);
                let mut new_inspector = Inspector::from_ref_inspector(&ins);

                new_inspector.request = request.to_string();
                new_inspector.response = response.to_string();
                new_inspector.modified_request = request.to_string();
                new_inspector.new_response = response.to_string();

                ins.selected = None;
                self.items.push(
                    new_inspector
                ); 
            }
        }
        self.items.retain(|x| { x.is_active});
    }

    pub fn add_inspector(&mut self, i: Inspector) {
        self.items.push(i);
        self.selected = self.items.len() - 1;
    }

    pub fn new(
        api_addr: Option<String>,
        api_port: Option<usize>,
        api_secret: Option<String>,
    ) -> Self {
        Self {
            api_addr,
            api_port,
            api_secret,
            ..Default::default()
        }
    }
}
