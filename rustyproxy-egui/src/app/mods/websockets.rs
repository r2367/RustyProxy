use crate::app::backend::dbutils::WebSockets;

#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
pub struct WebSocketViewer {
    pub id: usize,
    pub stream_rep: String,
    pub ssl: bool,
    pub messages: Vec<WebSockets>,
    pub is_minimized: bool,
}

impl WebSocketViewer {
    pub fn host(&self) -> &str {
        &self.stream_rep
    }

    pub fn status(&self) -> usize {
        200
    }

    pub fn remote_addr(&self) -> &str {
        &self.stream_rep
    }

    pub fn uri(&self) -> &str {
        ""
    }

    pub fn raw(&self) -> &str {
        /* to satisfy filter macro */
        &self.stream_rep
    }

    pub fn response(&self) -> &str {
        /* in order to satisfy the filter macro :) */
        ""
    }
}

#[macro_export]
macro_rules! websocket_ui {
    ($ui: expr, $w: expr, $websockets: expr) => {
        $ui.vertical(|ui| {
            egui::menu::bar(ui, |ui| {
                ui.with_layout(egui::Layout::top_down(egui::Align::RIGHT), |ui| {
                    ui.horizontal(|ui| {
                        if ui.button("x").clicked() {
                            $w.is_active = false;
                            ui.ctx().request_repaint();
                        }
                        ui.separator();
                        let bt = if $websockets.is_minimized { "+" } else { "-" };
                        if ui.button(bt).clicked() {
                            $websockets.is_minimized = !$websockets.is_minimized;
                            ui.ctx().request_repaint();
                        }
                        ui.separator();
                        ui.with_layout(egui::Layout::top_down(egui::Align::Center), |ui| {
                            ui.label(format!(
                                "Viewing WebSockets From {}",
                                $websockets.stream_rep
                            ));
                        });
                    });
                });
            });
            ui.separator();
            if !$websockets.is_minimized {
                egui::ScrollArea::vertical()
                    .id_source("bot ws viewer height")
                    .show(ui, |ui| {
                        egui::ScrollArea::horizontal()
                            .id_source("bot ws viewer width")
                            .show(ui, |ui| {
                                for msg in &$websockets.messages {
                                    ui.horizontal(|ui| {
                                        ui.label(format!("{}: ", msg.host()));
                                        ui.label(msg.data.to_string());
                                    });
                                    ui.separator();
                                }
                            });
                    });
            }
        });
    };
}
