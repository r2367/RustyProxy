use std::fmt::Write;

#[derive(Debug, Default, Clone, PartialEq)]
pub enum Ptype {
    #[default]
    GET,
    POST
}

#[derive(Debug, Default)]
pub struct Parameter {
    default_keyname: String,
    default_value: String,
    keyname: String,
    value: String,
    param_type: Ptype,
}

impl Parameter {
    pub fn display(&mut self, ui: &mut egui::Ui) {
        ui.columns(2, |c| {
            c[0].horizontal(|ui| {
                ui.label(if self.param_type == Ptype::GET { "[GET]" } else { "[POST]" });
                ui.label("name:");
                ui.text_edit_singleline(&mut self.keyname);
            });
            c[1].horizontal(|ui| {
                ui.label("value:");
                ui.text_edit_singleline(&mut self.value);
            });

        });
    }

    pub fn parse_one(s: &str, ptype: Ptype) -> (Self, &str) {
        let full = s.chars().take_while(|&x| x != '&').collect::<String>();
        let keyname = full.chars().take_while(|&x| x != '=').collect::<String>();
        let value = full.chars().skip_while(|&x| x != '=').skip(1).take_while(|&x| x != '&').collect::<String>();

        let len = if s.len() > full.len() {
            full.len() + 1
        } else {
            s.len()
        };
        
        (Self {
            default_keyname: keyname.to_string(),
            default_value: value.to_string(),
            keyname,
            value,
            param_type: ptype
        }, &s[len..])
        
    }
    
    pub fn parse_all(s: &str, ptype: Ptype) -> (Vec<Self>, &str) {
        
        let mut raw = s;
        let mut p: Parameter;
        let mut res = vec![];
        
        loop {
            (p, raw) = Parameter::parse_one(raw, ptype.clone());
            res.push(p);
            if raw.len() == 0 {
                break
            }
        }
        
        (res, raw)
    }

    pub fn original_string(&self) -> String {
        let mut out = String::new();

        write!(out, "{}", self.default_keyname).unwrap();

        if !self.default_value.is_empty() {
            write!(out, "={}", self.default_value).unwrap();
        }
        out
    }

    pub fn new_string(&self) -> String {
        let mut out = String::new();

        write!(out, "{}", self.keyname).unwrap();

        if !self.value.is_empty() {
            write!(out, "={}", self.value).unwrap();
        }
        out
    }
}