use crate::{row_openapi, tbl_dyn_col_openapi};
use egui_extras::{Column, TableBuilder};

#[derive(Default, Debug, serde::Deserialize, serde::Serialize)]
pub struct OpenApiWindow {
    path: String,
    content: Option<oas3::Spec>,
    last_seen_idx: usize,
    items_per_page: usize,
    info_active: bool,
    table_active: bool,
    is_minimized: bool,
}

impl OpenApiWindow {
    pub fn new(p: impl ToString) -> Self {
        let path = p.to_string();

        let parsing = oas3::from_path(&path);

        let content = match parsing {
            Ok(v) => Some(v),
            Err(e) => {
                dbg!(e);
                None
            }
        };

        Self {
            path,
            content,
            items_per_page: 10,
            info_active: true,
            table_active: true,
            ..Default::default()
        }
    }

    pub fn path(&self) -> &str {
        &self.path
    }

    pub fn is_minimized(&self) -> bool {
        self.is_minimized
    }

    pub fn minimized(&mut self, v: bool) {
        self.is_minimized = v;
    }

    pub fn display_info(&mut self, ui: &mut egui::Ui) {
        if let Some(c) = &self.content {
            ui.horizontal(|ui|{
                if ui.selectable_label(self.info_active, "☰ Info").clicked {
                    self.info_active = !self.info_active;
                }
                ui.separator();
                if ui.selectable_label(self.table_active, "☰ Table").clicked {
                    self.table_active = !self.table_active;
                }
                ui.separator();
            });

            ui.separator();
            if self.info_active {
                let summary = match &c.info.summary {
                    Some(v) => v.to_string(),
                    None => "No summary".to_string(),
                };

                let description = match &c.info.description {
                    Some(v) => v.to_string(),
                    None => "No description".to_string(),
                };

                ui.monospace(format!(
                    "title: {}, summary: {}, version: {}, description:\n{}",
                    &c.info.title, summary, &c.info.version, description
                ));

                ui.separator();
            }
        }
    }

    pub fn display(&mut self, ui: &mut egui::Ui) {
        if !self.is_minimized {
            let text_height = egui::TextStyle::Body.resolve(ui.style()).size;
            self.display_info(ui);
            if let Some(c) = &self.content {
                if self.table_active {
                    let mut items = c
                        .paths
                        .iter()
                        .enumerate()
                        .skip(self.last_seen_idx)
                        .take(self.items_per_page);

                    tbl_dyn_col_openapi!(
                        ui,
                        |body| {
                            body.rows(text_height, self.items_per_page, |mut tblrow| {
                                if let Some((idx, (path, value))) = items.next().as_ref() {
                                    row_openapi!(
                                        tblrow,
                                        (idx.to_string()),
                                        (if value.get.is_some() {
                                            "x".to_string()
                                        } else {
                                            "".to_string()
                                        }),
                                        (if value.put.is_some() {
                                            "x".to_string()
                                        } else {
                                            "".to_string()
                                        }),
                                        (if value.post.is_some() {
                                            "x".to_string()
                                        } else {
                                            "".to_string()
                                        }),
                                        (if value.head.is_some() {
                                            "x".to_string()
                                        } else {
                                            "".to_string()
                                        }),
                                        (if value.options.is_some() {
                                            "x".to_string()
                                        } else {
                                            "".to_string()
                                        }),
                                        (if value.patch.is_some() {
                                            "x".to_string()
                                        } else {
                                            "".to_string()
                                        }),
                                        (if value.delete.is_some() {
                                            "x".to_string()
                                        } else {
                                            "".to_string()
                                        }),
                                        (if value.trace.is_some() {
                                            "x".to_string()
                                        } else {
                                            "".to_string()
                                        }),
                                        (if let Some(summary) = &value.summary {
                                            summary.to_string()
                                        } else {
                                            "".to_string()
                                        }),
                                        (if let Some(desc) = &value.description {
                                            desc.to_string()
                                        } else {
                                            "".to_string()
                                        }),
                                        (path.to_string())
                                    );
                                }
                            });
                        },
                        self.last_seen_idx,
                        self.items_per_page,
                        c.paths.len(),
                        (Column::initial(10.0), "ID"),
                        (Column::initial(30.0), "GET"),
                        (Column::initial(30.0), "PUT"),
                        (Column::initial(30.0), "POST"),
                        (Column::initial(30.0), "HEAD"),
                        (Column::initial(30.0), "TRACE"),
                        (Column::initial(30.0), "DELETE"),
                        (Column::initial(30.0), "OPTIONS"),
                        (Column::initial(30.0), "PATCH"),
                        (Column::initial(50.0), "SUMMARY"),
                        (Column::initial(50.0), "DESCRIPTION"),
                        (Column::remainder(), "ENDPOINT")
                    );
                }

            } else {
                // parse the content with oas3
                ui.label("Could not parse the openapi spec :/");
            }
        }
    }
}
