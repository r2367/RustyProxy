#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct BrowserSettings {
    binary_path: Option<String>,
    proxy_addr_arg: Option<String>,
    other_args: Option<String>,
    pub binary_path_input: String,
    pub proxy_addr_input: String,
    pub other_args_input: String,
    #[serde(skip)]
    opened: bool,
}

impl BrowserSettings {
    pub fn binary_path(&self) -> &Option<String> {
        &self.binary_path
    }

    pub fn set_binary_path(&mut self, path: &str) {
        self.binary_path = if path.is_empty() {
            Some("chromium".to_string())
        } else {
            Some(path.to_string())
        };
    }

    pub fn proxy_addr_arg(&self) -> &Option<String> {
        &self.proxy_addr_arg
    }

    pub fn set_proxy_addr_arg(&mut self, proxy_arg: &str) {
        self.proxy_addr_arg = if proxy_arg.is_empty() {
            Some("--proxy-server=".to_string())
        } else {
            Some(proxy_arg.to_string())
        };
    }

    pub fn other_args(&self) -> &Option<String> {
        &self.other_args
    }

    pub fn set_other_args(&mut self, other: &str) {
        self.other_args = if other.is_empty() {
            None
        } else {
            Some(other.to_string())
        };
    }

    pub fn is_opened(&self) -> bool {
        self.opened
    }

    pub fn set_open(&mut self, opened: bool) {
        self.opened = opened;
    }

    pub fn command(&self, proxy_addr: &str, proxy_port: &str) -> String {
        match (self.binary_path(), self.proxy_addr_arg(), self.other_args()) {
            (Some(b), Some(p), o) => {
                if let Some(o) = o {
                    format!("{} {}http://{}:{} {}", b, p, proxy_addr, proxy_port, o,)
                } else {
                    format!("{} {}http://{}:{}", b, p, proxy_addr, proxy_port,)
                }
            }
            _ => {
                panic!("Should never hit");
            }
        }
    }

    pub fn display(&mut self, ui: &mut egui::Ui) {
        egui::Window::new("Browser Settings").title_bar(false).show(ui.ctx(), |ui| {
            egui::menu::bar(ui, |ui|{
                ui.label("Browser Settings");
                ui.with_layout(egui::Layout::top_down(egui::Align::RIGHT), |ui| {
                    ui.horizontal(|ui|{
                        if ui.button("x").clicked() {
                            self.set_open(false);
                            ui.ctx().request_repaint();
                        }
                    });
                });
            });
            ui.separator();
            ui.vertical(|ui|{

                ui.collapsing("How to use", |ui|{
                    let txt = r"- Command is the command to launch (defaults to chromium)
- Proxy address argument is the argument used to pass the proxy address, will be concatenated with http://proxy_addr:proxy_port, and defaults to --proxy-server=
- Other args are if you need any additionnal arguments, defaults to None";
                    ui.label(txt);
                });

                ui.horizontal(|ui| {
                    ui.label("Command:");
                    ui.text_edit_singleline(&mut self.binary_path_input);
                });

                ui.horizontal(|ui|{
                    ui.label("proxy addr arg:");
                    ui.text_edit_singleline(&mut self.proxy_addr_input);
                });

                ui.horizontal(|ui|{
                    ui.label("Other args:");
                    ui.text_edit_singleline(&mut self.other_args_input);
                });

                ui.horizontal(|ui|{
                    if ui.button("validate").clicked() {
                        self.set_binary_path(&self.binary_path_input.clone());
                        self.set_other_args(&self.other_args_input.clone());
                        self.set_proxy_addr_arg(&self.proxy_addr_input.clone());
                        self.set_open(false);
                    }

                    if ui.button("cancel").clicked() {
                        self.binary_path_input = String::new();
                        self.proxy_addr_input = String::new();
                        self.other_args_input = String::new();
                        self.set_open(false);
                    }
                });
            });
        });
    }
}

impl std::default::Default for BrowserSettings {
    fn default() -> Self {
        Self {
            binary_path: Some("chromium".to_string()),
            proxy_addr_arg: Some("--proxy-server=".to_string()),
            other_args: None,
            binary_path_input: "chromium".to_string(),
            proxy_addr_input: "--proxy-server=".to_string(),
            other_args_input: "".to_string(),
            opened: false,
        }
    }
}
