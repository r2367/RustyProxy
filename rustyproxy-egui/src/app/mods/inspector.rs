use crate::app::backend::batch_req;
use crate::app::backend::content_type::ContentType;
use crate::app::backend::dbutils::{HistLine, Inspectors};
use crate::app::mods::filter_cat::FilterCat;
use crate::app::mods::parameters::{Parameter, Ptype};
use crate::app::Range;
use crate::app::{apiutils, dbutils, HeaderMap};
use crate::app::{urldecode, urlencode};
use crate::{paginate, row, tbl_dyn_col};
use base64::{engine::general_purpose, Engine as _};
use egui::text_edit::TextEditOutput;
use egui_extras::{Column, TableBuilder};
use poll_promise::Promise;
use std::fmt::Write as fWrite;
use std::fs::File;
use std::io::{Read, Write};
use std::path::PathBuf;
use std::time::Duration;

#[derive(serde::Deserialize, serde::Serialize, Debug, Clone, PartialEq)]
pub enum DefaultToTrueBool {
    True,
    False,
}
impl Default for DefaultToTrueBool {
    fn default() -> Self {
        Self::True
    }
}
impl DefaultToTrueBool {
    pub fn as_bool(&self) -> bool {
        match self {
            DefaultToTrueBool::True => true,
            DefaultToTrueBool::False => false,
        }
    }
    pub fn switch(&mut self) -> Self {
        match self {
            DefaultToTrueBool::True => DefaultToTrueBool::False,
            DefaultToTrueBool::False => DefaultToTrueBool::True,
        }
    }
}
#[derive(serde::Deserialize, serde::Serialize, Debug, Clone, PartialEq)]
pub enum ActiveInspectorMenu {
    Default,
    Repeater,
    Intruder,
}
impl Default for ActiveInspectorMenu {
    fn default() -> Self {
        Self::Default
    }
}



#[derive(serde::Deserialize, serde::Serialize)]
#[serde(default)]
#[derive(Default)]
pub struct Inspector {
    pub id: usize,
    pub source: String,
    pub request: String,
    pub response: String,
    pub modified_request: String,
    pub new_response: String,
    #[serde(skip)]
    pub response_promise: Option<Promise<Result<String, reqwest::Error>>>,
    #[serde(skip)]
    pub server_promise: Option<Promise<Result<String, reqwest::Error>>>,
    pub ssl: bool,
    pub selected: Option<(usize, String, String, String, String, String)>,
    pub target: String,
    #[serde(skip)]
    pub active_window: ActiveInspectorMenu,
    #[serde(skip)]
    pub is_active: bool,
    #[serde(skip)]
    pub is_minimized: bool,
    #[serde(skip)]
    pub bf_payload: Vec<String>,
    pub bf_request: String,
    pub bf_results: Vec<batch_req::SuccessTuple>,
    #[serde(skip)]
    pub bf_promises: batch_req::VecPromiseType,
    #[serde(skip)]
    pub bf_payload_prepared: Vec<batch_req::Request>,
    #[serde(skip)]
    pub bf_current_page: usize,
    #[serde(skip)]
    pub bf_items_per_page: usize,
    #[serde(skip)]
    pub bf_filter_input: String,
    #[serde(skip)]
    pub bf_filter: Option<String>,
    #[serde(skip)]
    pub bf_filter_cat: Option<FilterCat>,
    pub repeater_as_hex: bool,
    pub url_encode_on_send: DefaultToTrueBool,
    pub pretty_json: DefaultToTrueBool,
    pub content_type: Option<ContentType>,
    pub request_data_already_pretty: bool,
    pub use_proxy: bool,
    pub proxy_addr: String,
    pub follow_redirect: bool,
    #[serde(skip)]
    pub parameters: Vec<Parameter>,
    #[serde(skip)]
    pub show_parameters: bool,
    pub parameters_initialized: bool,
}

impl Inspector {
    pub fn from_ref_inspector(ins: &Self) -> Self {
        Self {
            id: ins.id,
            source: ins.source.to_string(),
            request: ins.request.to_string(),
            response: ins.response.to_string(),
            modified_request: ins.modified_request.replace('\r', "\\r\\n"),
            new_response: ins.new_response.to_string(),
            bf_request: ins.bf_request.replace('\r', "\\r\\n"),
            ssl: ins.ssl,
            target: ins.target.to_string(),
            is_active: true,
            is_minimized: false,
            proxy_addr: ins.proxy_addr.to_string(),
            ..Default::default()
        }
    }

    pub fn from_histline(
        h: &HistLine,
        proxy_addr: &Option<String>,
        proxy_port: &Option<usize>,
    ) -> Self {
        let proxy_addr = format!(
            "http://{}:{}",
            proxy_addr.as_ref().unwrap(),
            proxy_port.as_ref().unwrap()
        );
        Self {
            id: h.id(),
            source: h.remote_addr().to_string(),
            request: h.raw().to_string(),
            response: h.response().to_string(),
            modified_request: h.raw().replace('\r', "\\r\\n"),
            new_response: h.response().to_string(),
            bf_request: h.raw().replace('\r', "\\r\\n"),
            ssl: h.ssl(),
            target: h.host().to_string(),
            is_active: true,
            is_minimized: false,
            proxy_addr: proxy_addr.to_string(),
            ..Default::default()
        }
    }
    pub fn from_inspector(
        i: &Inspectors,
        proxy_addr: &Option<String>,
        proxy_port: &Option<usize>,
    ) -> Self {
        let proxy_addr = format!(
            "http://{}:{}",
            proxy_addr.as_ref().unwrap(),
            proxy_port.as_ref().unwrap()
        );
        Self {
            id: i.id,
            source: "Collab".to_string(),
            request: i.request.to_string(),
            response: i.response.to_string(),
            modified_request: i.modified_request.to_string(),
            new_response: i.new_response.to_string(),
            ssl: i.ssl,
            target: i.target.to_string(),
            bf_request: i.bf_request.to_string(),
            is_active: true,
            is_minimized: false,
            proxy_addr,
            ..Default::default()
        }
    }

    pub fn check_for_get_params(&self) -> String {
        let first_line = dbg!(self.modified_request.split("\\r\\n").take(1).collect::<String>());

        dbg!(first_line.chars().skip_while(|&x| x != '?').skip(1).take_while(|&x| x != ' ').collect::<String>())
    }

    pub fn check_for_post_params(&self) -> String {
        dbg!(self.modified_request.split("\\r\\n\n\\r\\n\n").skip(1).collect::<String>())
    }

    pub fn get_parameters(&mut self) {
        if self.modified_request.starts_with("GET") {
            /* check for get params */
            let (mut v, _) = Parameter::parse_all(&self.check_for_get_params(), Ptype::GET);
            self.parameters.append(&mut v);
        } else if self.modified_request.starts_with("POST") || self.modified_request.starts_with("PATCH") {
            /* check for get AND post params */
            let (mut v, _) = Parameter::parse_all(&self.check_for_get_params(), Ptype::GET);
            self.parameters.append(&mut v);
            let (mut v, _) = Parameter::parse_all(&self.check_for_post_params(), Ptype::POST);
            self.parameters.append(&mut v);
        }
    }

    pub fn display_parameters(&mut self, ui: &mut egui::Ui) {
        if !self.parameters_initialized {
            self.get_parameters();
            self.parameters_initialized = true;
        }

        ui.group(|ui| {
            if self.parameters.len() == 0 {
                ui.label("No params parsed");
            } else {
                ui.vertical(|ui| {
                    for p in self.parameters.iter_mut() {
                        p.display(ui);
                    }
                });
            }
        });
        
        
    }

    pub fn display(
        &mut self,
        ui: &mut egui::Ui,
        api_addr: Option<String>,
        api_port: Option<usize>,
        api_secret: Option<String>,
    ) -> bool {
        let mut clicked = false;
        egui::menu::bar(ui, |ui| {
            if ui
                .selectable_label(
                    self.active_window == ActiveInspectorMenu::Default,
                    "☰ Default",
                )
                .clicked()
            {
                self.active_window = ActiveInspectorMenu::Default;
            }
            ui.separator();
            if ui
                .selectable_label(
                    self.active_window == ActiveInspectorMenu::Repeater,
                    "☰ Modify",
                )
                .clicked()
            {
                self.active_window = ActiveInspectorMenu::Repeater;
            }
            ui.separator();
            if ui
                .selectable_label(
                    self.active_window == ActiveInspectorMenu::Intruder,
                    "☰ Bruteforce",
                )
                .clicked()
            {
                self.active_window = ActiveInspectorMenu::Intruder;
            }
            ui.separator();
            if ui.button("✉ Push to server").clicked() {
                /* send to server */
                let url = format!("{}:{}", api_addr.clone().unwrap(), api_port.unwrap());
                let secret = api_secret.clone().unwrap();
                let ins = dbutils::Inspectors::from_inspector(self);

                self.server_promise.get_or_insert_with(|| {
                    return Promise::spawn_thread("ins_push", move || {
                        apiutils::push_inspector_to_api(&ins, &url, &secret)
                    });
                });
            }
            ui.separator();
            ui.with_layout(egui::Layout::top_down(egui::Align::RIGHT), |ui| {
                ui.horizontal(|ui| {
                    if ui.button("x").clicked() {
                        self.is_active = false;
                        ui.ctx().request_repaint();
                    }
                    ui.separator();
                    let bt = if self.is_minimized { "+" } else { "-" };
                    if ui.button(bt).clicked() {
                        self.is_minimized = !self.is_minimized;
                        ui.ctx().request_repaint();
                    }
                    ui.separator();
                    ui.label(format!("{} <-> {}", &self.source, &self.target));
                    ui.label("💻 ");
                    ui.separator();
                    if self.ssl {
                        ui.label("true");
                    } else {
                        ui.label("false");
                    }
                    ui.label("ssl: ");
                    ui.separator();
                    ui.with_layout(egui::Layout::top_down(egui::Align::Center), |ui| {
                        ui.label(format!("Viewing #{}", self.id));
                    });
                });
            });
        });
        if let Some(p) = &self.server_promise {
            if let Some(Ok(_s)) = p.ready() {
                self.server_promise = None;
                ui.ctx().request_repaint();
            }
        }
        ui.separator();
        if !self.is_minimized {
            match self.active_window {
                ActiveInspectorMenu::Repeater => {
                    egui::menu::bar(ui, |ui| {
                        if ui.button("⚠ Reset").clicked() {
                            self.modified_request =
                                self.request.to_string().replace('\r', "\\r\\n");
                            self.new_response = self.response.to_string();
                            self.request_data_already_pretty = false;
                        }
                        ui.separator();
                        if ui.button("📩 Save Modified Request").clicked() {
                            if let Some(path) = rfd::FileDialog::new().save_file() {
                                save_content_to_file(
                                    path,
                                    &self.modified_request.replace("\\r\\n", "\r"),
                                );
                            }
                        }
                        ui.separator();
                        if ui.button("📋 Copy as Curl").clicked() {
                            copy_as_curl(
                                ui,
                                &self.modified_request.replace("\\r\\n", "\r"),
                                self.ssl,
                                &self.target,
                            );
                        }
                        ui.separator();
                        if ui.button("✉ Send").clicked() {
                            /* Parse request */
                            self.prepare_and_send_request();
                            ui.ctx().request_repaint();
                        }
                        ui.separator();
                        if ui.button("⏏ Open new window").clicked() {
                            self.selected = Some((
                                self.id + 1000000,
                                "".to_string(),
                                "".to_string(),
                                "".to_string(),
                                self.new_response.to_string(),
                                "".to_string(),
                            ));
                            clicked = true;
                        };
                        ui.separator();
                        if let Some(p) = &self.response_promise {
                            if let Some(Ok(s)) = p.ready() {
                                self.new_response = s.to_string();
                                self.response_promise = None;
                                ui.ctx().request_repaint();
                            }
                        }
                    });
                    ui.separator();
                    egui::menu::bar(ui, |ui| {
                        if ui
                            .selectable_label(self.repeater_as_hex, "View as hex")
                            .clicked()
                        {
                            self.repeater_as_hex = !self.repeater_as_hex;
                        }
                        ui.separator();
                        if ui
                            .selectable_label(self.pretty_json.as_bool(), "Pretty JSON")
                            .clicked()
                        {
                            self.pretty_json = self.pretty_json.switch();
                            if !self.pretty_json.as_bool() {
                                /* user wants to go back to unformated */
                                if let Ok(s) = ContentType::flatten_request(&self.modified_request)
                                {
                                    self.modified_request = s;
                                    self.request_data_already_pretty = false;
                                }
                            }
                        }
                        ui.separator();
                        if ui
                            .selectable_label(
                                self.url_encode_on_send.as_bool(),
                                "UrlEncode on send",
                            )
                            .clicked()
                        {
                            self.url_encode_on_send = self.url_encode_on_send.switch();
                        }
                        ui.separator();
                        if ui
                            .selectable_label(self.use_proxy, "Go through proxy")
                            .clicked()
                        {
                            self.use_proxy = !self.use_proxy;
                        }
                        ui.separator();
                        if ui
                            .selectable_label(self.follow_redirect, "Follow redirections")
                            .clicked()
                        {
                            self.follow_redirect = !self.follow_redirect;
                        }
                        ui.separator();
                        if ui.selectable_label(self.show_parameters, "Params tools").clicked() {
                            self.show_parameters = !self.show_parameters;
                        }
                        ui.separator();
                        let status = self.response_promise.is_some();
                        let text = format!("Status: {}", if status { "Loading" } else { "Done" });
                        ui.with_layout(egui::Layout::top_down(egui::Align::RIGHT), |ui| {
                            ui.horizontal(|ui| {
                                let _ = ui.selectable_label(!status, text);
                                ui.separator();
                                ui.label(format!("Response size: {}", self.new_response.len()));
                            });
                        });
                    });
                    ui.separator();
                    egui::ScrollArea::both()
                        .id_source("top_repeater")
                        .show(ui, |ui| {
                            if self.content_type.is_none() {
                                self.content_type =
                                    Some(ContentType::from_raw_request(&self.modified_request));
                            }
                            let output = match (
                                self.pretty_json.as_bool(),
                                self.show_parameters,
                                &self.content_type,
                                self.request_data_already_pretty,
                            ) {
                                (true, _, Some(ContentType::Json), false) => {
                                    /* data is not pretty, do it */
                                    if let Ok(pretty) =
                                        ContentType::pretty_request(&self.modified_request)
                                    {
                                        self.modified_request = pretty;
                                        self.request_data_already_pretty = true;
                                    }
                                    ui.columns(1, |c|{
                                        code_edit_ui(&mut c[0], &mut self.modified_request)
                                    })
                                },
                                (_, true, Some(ContentType::Other(_)) | Some(ContentType::Empty) | None, _) => {
                                    ui.columns(2, |c| {
                                        let out = code_edit_ui(&mut c[0], &mut self.modified_request);
                                        self.display_parameters(&mut c[1]);
                                        out
                                    })
                                },
                                _ => {
                                    ui.columns(1, |c| { code_edit_ui(&mut c[0], &mut self.modified_request) } )
                                },
                            };
                            if output.response.has_focus()
                                && ui.input_mut(|i| {
                                    i.consume_key(
                                        egui::Modifiers {
                                            command: true,
                                            ..Default::default()
                                        },
                                        egui::Key::Space,
                                    )
                                })
                            {
                                dbg!(self.prepare_and_send_request());
                            }
                            if output.response.changed() {
                                self.request_data_already_pretty = false;
                            }

                            ui.separator();
                            if self.repeater_as_hex {
                                ui.columns(2, |c| {
                                    code_view_ui(
                                        &mut c[0],
                                        &rhexdump::rhexdumps!(self.new_response.as_bytes()),
                                    );
                                    code_view_ui(
                                        &mut c[1],
                                        &self.new_response.to_string(),
                                    );
                                });
                                
                            } else {
                                let output = code_view_ui(ui, &self.new_response.to_string());
                                if ui.input_mut(|i| {
                                    i.consume_key(
                                        egui::Modifiers {
                                            command: true,
                                            shift: true,
                                            ..Default::default()
                                        },
                                        egui::Key::B,
                                    )
                                }) {
                                    if let Some(text_cursor_range) = output.cursor_range {
                                        use egui::TextBuffer as _;
                                        let selected_chars =
                                            text_cursor_range.as_sorted_char_range();
                                        let copy = &mut self.new_response.clone();
                                        let selected_text = copy.char_range(selected_chars.clone());
                                        let decoded =
                                            general_purpose::STANDARD.decode(selected_text);
                                        if let Ok(txt) = decoded {
                                            let txt = String::from_utf8_lossy(&txt).to_string();
                                            self.new_response
                                                .delete_char_range(selected_chars.clone());
                                            self.new_response
                                                .insert_text(&txt, selected_chars.start);
                                        }
                                    }
                                }
                            }
                        });
                }
                ActiveInspectorMenu::Default => {
                    egui::menu::bar(ui, |ui| {
                        if ui.button("📩 Save Request").clicked() {
                            if let Some(path) = rfd::FileDialog::new().save_file() {
                                save_content_to_file(path, &self.request.replace("\r\n", "\r"));
                            }
                        }
                        ui.separator();
                        if ui.button("📩 Save All").clicked() {
                            if let Some(path) = rfd::FileDialog::new().save_file() {
                                save_content_to_file(
                                    path,
                                    &serde_json::to_string(&self).unwrap_or(
                                        "serde_json::to_string(&inspected) failed".to_string(),
                                    ),
                                );
                            }
                        }
                        ui.separator();
                        if ui.button("📋 Copy as Curl").clicked() {
                            copy_as_curl(ui, &self.request, self.ssl, &self.target);
                        }
                        ui.separator();
                        if ui.button("⏏ Open new window").clicked() {
                            self.selected = Some((
                                self.id + 1000000,
                                "".to_string(),
                                "".to_string(),
                                "".to_string(),
                                self.new_response.to_string(),
                                "".to_string(),
                            ));
                            clicked = true;
                        };
                        ui.separator();
                    });
                    ui.separator();
                    egui::ScrollArea::both()
                        .id_source("top_default")
                        .show(ui, |ui| {
                            let output = code_view_ui(ui, &self.request);
                            if let Some(text_cursor_range) = output.cursor_range {
                                if ui.input_mut(|i| {
                                    i.consume_key(
                                        egui::Modifiers {
                                            command: true,
                                            shift: true,
                                            ..Default::default()
                                        },
                                        egui::Key::B,
                                    )
                                }) {
                                    use egui::TextBuffer as _;
                                    let selected_chars = text_cursor_range.as_sorted_char_range();
                                    let copy = &mut self.request.clone();
                                    let selected_text = copy.char_range(selected_chars.clone());
                                    let decoded = general_purpose::STANDARD.decode(selected_text);
                                    if let Ok(txt) = decoded {
                                        let txt = String::from_utf8_lossy(&txt).to_string();
                                        self.request.delete_char_range(selected_chars.clone());
                                        self.request.insert_text(&txt, selected_chars.start);
                                    }
                                }
                            }
                            ui.separator();
                            let output = code_view_ui(ui, &self.response);
                            if let Some(text_cursor_range) = output.cursor_range {
                                if ui.input_mut(|i| {
                                    i.consume_key(
                                        egui::Modifiers {
                                            command: true,
                                            shift: true,
                                            ..Default::default()
                                        },
                                        egui::Key::B,
                                    )
                                }) {
                                    use egui::TextBuffer as _;
                                    let selected_chars = text_cursor_range.as_sorted_char_range();
                                    let copy = &mut self.response.clone();
                                    let selected_text = copy.char_range(selected_chars.clone());
                                    let decoded = general_purpose::STANDARD.decode(selected_text);
                                    if let Ok(txt) = decoded {
                                        let txt = String::from_utf8_lossy(&txt).to_string();
                                        self.response.delete_char_range(selected_chars.clone());
                                        self.response.insert_text(&txt, selected_chars.start);
                                    }
                                }
                            }
                        });
                }
                ActiveInspectorMenu::Intruder => {
                    egui::menu::bar(ui, |ui| {
                        if ui.button("⚠ Reset").clicked() {
                            self.bf_request = self.request.to_string().replace('\r', "\\r\\n");
                        }
                        ui.separator();
                        if ui.button("📩 Save Modified Request").clicked() {
                            if let Some(path) = rfd::FileDialog::new().save_file() {
                                save_content_to_file(
                                    path,
                                    &self.modified_request.replace("\\r\\n", "\r"),
                                );
                            }
                        }
                        ui.separator();
                        if ui.button("✉ Send").clicked() {
                            /* Actually start bruteforcing */
                            let requests: Vec<String> = self
                                .bf_payload
                                .iter()
                                .map(|p| {
                                    self.bf_request
                                        .replace("\\r\\n", "\r")
                                        .replace("$[PAYLOAD]$", p)
                                })
                                .collect();
                            self.bf_payload_prepared = batch_req::Request::from_strings(
                                requests,
                                self.ssl,
                                self.target.to_string(),
                            );
                            let pa = if self.use_proxy {
                                Some(self.proxy_addr.to_string())
                            } else {
                                None
                            };
                            batch_req::BatchRequest::run(
                                &self.bf_payload_prepared,
                                &mut self.bf_promises,
                                pa,
                            );
                        }
                        ui.separator();
                        if ui.button("☰ Load Payloads from File").clicked() {
                            if let Some(path) = rfd::FileDialog::new().pick_file() {
                                if let Some(payload) = load_content_from_file(path) {
                                    self.bf_payload = payload
                                        .split('\n')
                                        .map(|v| v.trim_end().to_string())
                                        .collect::<Vec<String>>();
                                }
                            }
                        }
                        ui.separator();
                        if ui
                            .selectable_label(self.use_proxy, "Go through proxy")
                            .clicked()
                        {
                            self.use_proxy = !self.use_proxy;
                        }
                        ui.separator();
                        ui.label(format!("Number of request: {}", self.bf_payload.len()));
                        ui.separator();
                    });
                    ui.separator();
                    ui.columns(1, |c| {code_edit_ui(&mut c[0], &mut self.bf_request)});
                    ui.separator();
                    egui::ScrollArea::both().show(ui, |ui| {
                        let text_height = egui::TextStyle::Body.resolve(ui.style()).size;
                        tbl_dyn_col!(
                            ui,
                            |mut body| {
                                self.bf_promises.retain(|prom| {
                                    if let Some(vr) = prom.ready() {
                                        for r in vr {
                                            match r {
                                                Ok((idx, version, status, headers, text, duration)) => {
                                                    self.bf_results.push((
                                                        *idx,
                                                        version.to_string(),
                                                        status.to_string(),
                                                        headers.to_string(),
                                                        text.to_string(),
                                                        *duration
                                                    ))
                                                }
                                                Err((idx, e)) => self.bf_results.push((
                                                    *idx,
                                                    "SRVBUG".to_string(),
                                                    e.to_string(),
                                                    "SRVBUG".to_string(),
                                                    "SRVBUG".to_string(),
                                                    Duration::from_secs(0),
                                                )),
                                            }
                                        }
                                    }
                                    prom.ready().is_none()
                                });
                                let range = paginate!(
                                    self.bf_current_page,
                                    self.bf_items_per_page,
                                    self.bf_results.len()
                                );
                                for r in &self.bf_results[range] {
                                    let (idx, version, status, headers, resp_body, duration) = r;
                                    let payload = self.bf_payload[*idx].to_string();
                                    body.row(text_height, |mut row| {
                                        row!(
                                            row,
                                            {
                                                self.selected = Some((
                                                    idx.to_owned(),
                                                    version.to_string(),
                                                    status.to_string(),
                                                    headers.to_string(),
                                                    resp_body.to_string(),
                                                    payload.to_string(),
                                                ));
                                                clicked = true;
                                            },
                                            (idx.to_string(), true),
                                            (&payload, true),
                                            (format!("{:?}", &duration), true),
                                            (resp_body.len().to_string(), true),
                                            (status, true)
                                        );
                                    });
                                }
                            },
                            self.bf_current_page,
                            self.bf_items_per_page,
                            self.bf_results.len(),
                            self.bf_filter,
                            self.bf_filter_cat,
                            &mut self.bf_filter_input,
                            (Column::exact(60.0), true),
                            (Column::exact(400.0), true),
                            (Column::exact(100.0), true),
                            (Column::exact(60.0), true),
                            (Column::exact(60.0), true) //Size::exact(60.0)
                        );
                    });
                }
            }
        }
        clicked
    }

    fn prepare_and_send_request(&mut self) {
        

        if self.show_parameters {
            for p in &self.parameters {
                self.modified_request = self.modified_request.replacen(&p.original_string(), &p.new_string(), 1);
            }
        }
        
        self.parameters_initialized = false;
        self.parameters = vec![];

        let request = self.modified_request.replace("\\r\\n", "\r");
        let method = request.split(' ').take(1).collect::<String>();
        let mut uri = request.split(' ').skip(1).take(1).collect::<String>();
        if self.url_encode_on_send.as_bool() {
            if let Some(pos) = uri.find('?') {
                let to_encode = &uri[pos + 1..];
                let uri_base = &uri[..pos];
                if to_encode.contains('=') {
                    let mut encoded_args = String::new();
                    let mut first = true;
                    for params in to_encode.split('&') {
                        let splited = params.split('=').collect::<Vec<&str>>();
                        if splited.len() == 2 {
                            let name = splited[0];
                            let value = splited[1];
                            encoded_args += &format!(
                                "{}{}={}",
                                if !first { "&" } else { "" },
                                name,
                                urlencode(value)
                            );
                        } else {
                            let value = splited[0];
                            encoded_args +=
                                &format!("{}{}", if !first { "&" } else { "" }, urlencode(value));
                        }
                        first = false;
                    }
                    uri = format!("{}?{}", uri_base, &encoded_args);
                } else {
                    uri = format!("{}?{}", uri_base, urlencode(to_encode));
                }
            }
        }
        let url = format!(
            "{}{}{}",
            if self.ssl { "https://" } else { "http://" },
            self.target,
            uri
        );

        let pos = request.find("\r\n\r\n").unwrap();
        let body = request
            .chars()
            .skip(pos + 4)
            .collect::<String>()
            .as_bytes()
            .to_vec();
        let mut headers = HeaderMap::new();
        for header in request
            .split("\r\n")
            .skip(1)
            .map_while(|x| if !x.is_empty() { Some(x) } else { None })
            .collect::<Vec<&str>>()
        {
            let name = reqwest::header::HeaderName::from_bytes(
                header
                    .split(':')
                    .take(1)
                    .collect::<String>()
                    .trim()
                    .as_bytes(),
            )
            .unwrap();
            let header_value_raw = header
                .chars()
                .skip_while(|&c| c != ':')
                .skip(2) //we are removing the : and the space after it
                .collect::<String>();
            let value =
                reqwest::header::HeaderValue::from_bytes(header_value_raw.trim().as_bytes())
                    .unwrap_or(reqwest::header::HeaderValue::from_static(
                        "INVALID_HEADERVALUE",
                    ));
            headers.insert(name, value);
        }
        /* Actually send the request */
        let use_proxy = self.use_proxy;
        let proxy_addr = self.proxy_addr.to_string();
        dbg!(&proxy_addr);
        let follow_redirect = self.follow_redirect;
        self.response_promise.get_or_insert_with(|| {
            return Promise::spawn_thread("rq", move || {
                let mut builder = reqwest::blocking::Client::builder()
                    .danger_accept_invalid_certs(true)
                    .default_headers(headers)
                    .brotli(true)
                    .gzip(true)
                    .deflate(true);

                if use_proxy {
                    builder = builder.proxy(reqwest::Proxy::all(&proxy_addr)?);
                }

                if follow_redirect {
                    builder = builder.redirect(reqwest::redirect::Policy::default());
                }
                let cli = builder.build().unwrap();

                cli.request(
                    reqwest::Method::from_bytes(method.as_bytes()).unwrap_or(reqwest::Method::GET),
                    url,
                )
                .body(body)
                .send()
                .map(move |r| {
                    let headers: String =
                        r.headers().iter().fold(String::new(), |mut output, item| {
                            let (key, value) = item;
                            let value = value.to_str().unwrap_or("INVALID HEADERVALUE");
                            let _ = write!(output, "{key}: {value}\r\n");
                            output
                        });

                    format!(
                        "{:?} {} {}\r\n{}\r\n{}",
                        r.version(),
                        r.status().as_str(),
                        r.status()
                            .canonical_reason()
                            .unwrap_or("INVALID CANONICAL_REASON"),
                        headers,
                        r.text().unwrap_or("INVALID BODY".to_string())
                    )
                })
            });
        });
    }
}
impl std::fmt::Debug for Inspector {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "Inspector #{} ", self.id)?;
        write!(f, "source: {} ", self.source)?;
        write!(f, "is_active: {} ", self.is_active)?;
        write!(f, "is_minimized: {} ", self.is_minimized)?;
        write!(f, "active_window: {:?} ", self.active_window)?;
        write!(f, "target: {}", self.target)?;
        Ok(())
    }
}
pub fn load_content_from_file(path: PathBuf) -> Option<String> {
    if let Ok(mut fd) = File::open(path) {
        let mut out = vec![];
        if fd.read_to_end(&mut out).is_ok() {
            return Some(String::from_utf8_lossy(&out).trim_end().to_string());
        }
    }
    None
}
pub fn save_content_to_file(path: PathBuf, content: &String) -> bool {
    if let Ok(mut fd) = File::create(path) {
        if write!(fd, "{}", content).is_ok() {
            return true;
        }
    }
    false
}
pub fn copy_as_curl(ui: &mut egui::Ui, content: &str, ssl: bool, target: &String) {
    let method = content.split(' ').take(1).collect::<String>();
    let uri = content.split(' ').skip(1).take(1).collect::<String>();
    let url = format!(
        "{}{}{}",
        if ssl { "https://" } else { "" },
        if ssl {
            target.to_string()
        } else {
            "".to_string()
        },
        uri
    );
    let pos = content.find("\r\n\r\n").unwrap();
    let body = content.chars().skip(pos + 4).collect::<String>();
    let mut scurl = format!("curl '{}' -X '{}'", url, method);
    for header in content
        .split("\r\n")
        .skip(1)
        .map_while(|x| if !x.is_empty() { Some(x) } else { None })
        .collect::<Vec<&str>>()
    {
        write!(scurl, " -H '{}'", &header).unwrap();
    }
    write!(scurl, " --data '{}'", &body).unwrap();
    ui.output_mut(|o| o.copied_text = scurl.to_string());
}
pub fn code_view_ui(ui: &mut egui::Ui, mut code: &str) -> TextEditOutput {
    let output = egui::TextEdit::multiline(&mut code)
        .font(egui::TextStyle::Monospace) // for cursor height
        .code_editor()
        .desired_width(f32::INFINITY)
        .desired_rows(10)
        .clip_text(true)
        .interactive(true)
        .lock_focus(false)
        .show(ui);
    output
}
pub fn code_edit_ui(ui: &mut egui::Ui, code: &mut String) -> TextEditOutput {
    let output = egui::TextEdit::multiline(code)
        .font(egui::TextStyle::Monospace) // for cursor height
        .code_editor()
        .desired_rows(10)
        .clip_text(true)
        .interactive(true)
        .lock_focus(true)
        .show(ui);
    if let Some(text_cursor_range) = output.cursor_range {
        if ui.input_mut(|i| i.consume_key(egui::Modifiers::COMMAND, egui::Key::Y)) {
            use egui::TextBuffer as _;
            let selected_chars = text_cursor_range.as_sorted_char_range();
            let copy = code.clone();
            let selected_text = copy.char_range(selected_chars.clone());
            let new_text = urlencode(selected_text);
            code.delete_char_range(selected_chars.clone());
            code.insert_text(&new_text, selected_chars.start);
        }
        if ui.input_mut(|i| {
            i.consume_key(
                egui::Modifiers {
                    command: true,
                    shift: true,
                    ..Default::default()
                },
                egui::Key::Y,
            )
        }) {
            use egui::TextBuffer as _;
            let selected_chars = text_cursor_range.as_sorted_char_range();
            let copy = code.clone();
            let selected_text = copy.char_range(selected_chars.clone());
            let new_text = urldecode(selected_text);
            if let Ok(txt) = new_text {
                code.delete_char_range(selected_chars.clone());
                code.insert_text(&txt, selected_chars.start);
            }
        }
        if ui.input_mut(|i| {
            i.consume_key(
                egui::Modifiers {
                    command: true,
                    shift: true,
                    ..Default::default()
                },
                egui::Key::B,
            )
        }) {
            use egui::TextBuffer as _;
            let selected_chars = text_cursor_range.as_sorted_char_range();
            let copy = &mut code.clone();
            let selected_text = copy.char_range(selected_chars.clone());
            let decoded = general_purpose::STANDARD.decode(selected_text);
            if let Ok(txt) = decoded {
                let txt = String::from_utf8_lossy(&txt).to_string();
                code.delete_char_range(selected_chars.clone());
                code.insert_text(&txt, selected_chars.start);
            }
        }
        if ui.input_mut(|i| i.consume_key(egui::Modifiers::COMMAND, egui::Key::B)) {
            use egui::TextBuffer as _;
            let selected_chars = text_cursor_range.as_sorted_char_range();
            let copy = &mut code.clone();
            let selected_text = copy.char_range(selected_chars.clone());
            let mut encoded = String::new();
            general_purpose::STANDARD.encode_string(selected_text, &mut encoded);
            code.delete_char_range(selected_chars.clone());
            code.insert_text(&encoded, selected_chars.start);
        }
    }
    output
}
