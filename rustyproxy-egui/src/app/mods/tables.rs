#[macro_export]
macro_rules! tbl_dyn_col {
    ( $ui: expr, $closure: expr, $current_page: expr, $items_per_page: expr, $items_number: expr, $filter: expr, $filter_cat: expr, $filter_input: expr, $(($cols:expr, $active:expr)),*) => {
        let mut tbl = TableBuilder::new($ui)
            .auto_shrink([false, true])
            .striped(true)
            .cell_layout(egui::Layout::left_to_right(egui::Align::Center))
            .resizable(true)
            .vscroll(false)
            .stick_to_bottom(false);
        $(
            if $active {
                tbl = tbl.column($cols);
            }
        )*
        tbl.body($closure);
        $ui.separator();
        egui::menu::bar($ui, |ui| {
            if $items_number > $items_per_page {
                let lbl = format!("Current page: {}", $current_page);
                ui.label(lbl);
                ui.label("⬌ Items per page: ");
                ui.add(
                    egui::Slider::new(
                        &mut $items_per_page,
                        (10 as usize)..=(40 as usize),
                    )
                    .logarithmic(true),
                );
            }

            ui.separator();
            ui.with_layout(egui::Layout::top_down(egui::Align::RIGHT), |ui| {
                ui.horizontal(|ui| {
                    if ui.button(">").clicked() {
                        if $items_number - ($current_page * $items_per_page)
                            > $items_per_page
                        {
                            $current_page += 1;
                        }
                    }
                    if ui.button("<").clicked() {
                        if $current_page != 0 {
                            $current_page -= 1;
                        }
                    };
                    ui.separator();
                    ui.monospace($items_number.to_string());
                    ui.label("Number of results: ");
                    ui.separator();
                });
            });
        });
    };
}

#[macro_export]
macro_rules! tbl_dyn_col_v2 {
    ( $ui: expr, $closure: expr, $last_seen_idx:expr, $last_seen_idx_filtered: expr, $items_per_page: expr, $items_number: expr, $filter: expr, $filter_cat: expr, $filter_input: expr, $(($cols:expr, $active:expr, $headername: expr)),*) => {
        let mut tbl = TableBuilder::new($ui)
            .auto_shrink([false, true])
            .striped(true)
            .cell_layout(egui::Layout::left_to_right(egui::Align::Center))
            .resizable(true)
            .vscroll(false)
            .stick_to_bottom(false);
        $(
            if $active {
                tbl = tbl.column($cols);
            }
        )*
        let tbl = tbl.header(20.0, |mut header| {
                $(
                    if $active {
                        header.col(|ui| {
                            ui.monospace($headername);
                        });
                    }
                )*
            });
        tbl.body($closure);
        $ui.separator();
        egui::menu::bar($ui, |ui| {
            if $items_number > $items_per_page {
                let lbl = format!("Current range: {} - {}", $last_seen_idx, $last_seen_idx + $items_per_page);
                ui.label(lbl);
                ui.label("⬌ Items per page: ");
                ui.add(
                    egui::Slider::new(
                        &mut $items_per_page,
                        (10 as usize)..=(40 as usize),
                    )
                    .logarithmic(true),
                );
            }
            ui.separator();
            let menu = if $filter_cat.is_some(){
                format!("Filter by: {:?}", $filter_cat.as_ref().unwrap())
            }
            else {
                "Filter by ⏷".into()
            };
            ui.menu_button(menu, |ui| {
                if ui.button("Host").clicked() {
                    $filter_cat = Some(FilterCat::Host);
                    $last_seen_idx = 0;
                    $filter_input.clear();
                    $filter = None;
                }
                if ui.button("Code").clicked() {
                    $filter_cat = Some(FilterCat::Code);
                    $last_seen_idx = 0;
                    $filter_input.clear();
                    $filter = None;
                }
                if ui.button("Source").clicked() {
                    $filter_cat = Some(FilterCat::Source);
                    $last_seen_idx = 0;
                    $filter_input.clear();
                    $filter = None;
                }
                if ui.button("Path").clicked() {
                    $filter_cat = Some(FilterCat::Path);
                    $last_seen_idx = 0;
                    $filter_input.clear();
                    $filter = None;
                }
                if ui.button("Generic").clicked() {
                    $filter_cat = Some(FilterCat::Generic);
                    $last_seen_idx = 0;
                    $filter_input.clear();
                    $filter = None;
                }
            });

            let response = ui.text_edit_singleline($filter_input);
            if response.lost_focus() && ui.input(|i| i.key_pressed(egui::Key::Enter)) {
                if $filter_input != "" {
                    $filter = Some($filter_input.to_owned());
                } else {
                    $filter = None;
                }
            }
            ui.separator();
            if ui.button("clear").clicked() {
                $filter_input.clear();
                $filter = None;
            }
            ui.separator();
            ui.with_layout(egui::Layout::top_down(egui::Align::RIGHT), |ui| {
                ui.horizontal(|ui| {
                    if ui.button(">").clicked() {
                        match $filter.is_some() {
                            true => $last_seen_idx_filtered += $items_per_page,
                            false => $last_seen_idx += $items_per_page,
                        }
                    }
                    if ui.button("<").clicked() {
                        match ($filter.is_some(), $last_seen_idx_filtered as i32 - $items_per_page as i32, $last_seen_idx as i32 - $items_per_page as i32) {
                            (true, 0.., _) => $last_seen_idx_filtered -= $items_per_page, // $last_seen_idx_filtered >= $items_per_page
                            (true, ..=-1, _) => $last_seen_idx_filtered = 0, // $last_seen_idx_filtered < $items_per_page
                            (false, _, 0..) => $last_seen_idx -= $items_per_page, // $last_seen_idx >= $items_per_page
                            (false, _, ..=-1) => $last_seen_idx = 0, // $last_seen_idx < $items_per_page
                        }
                    }
                    ui.separator();
                    ui.monospace($items_number.to_string());
                    ui.label("Total (unfiltered): ");
                    ui.separator();
                });
            });
        });
    };
}

#[macro_export]
macro_rules! tbl_dyn_col_openapi {
    ( $ui: expr, $closure: expr, $last_seen_idx:expr, $items_per_page: expr, $items_number: expr, $(($cols:expr, $headername: expr)),*) => {
        let mut tbl = TableBuilder::new($ui)
            .auto_shrink([false, true])
            .striped(true)
            .cell_layout(egui::Layout::left_to_right(egui::Align::Center))
            .resizable(true)
            .vscroll(true)
            .stick_to_bottom(false);
        $(
            tbl = tbl.column($cols);
        )*
        let tbl = tbl.header(20.0, |mut header| {
                $(
                    header.col(|ui| {
                        ui.monospace($headername);
                    });
                )*
            });
        tbl.body($closure);
        $ui.separator();
        $ui.with_layout(egui::Layout::top_down(egui::Align::RIGHT), |ui| {
            ui.horizontal(|ui| {
                if ui.button(">").clicked() {
                        $last_seen_idx += $items_per_page;
                }
                if ui.button("<").clicked() {
                    match ($last_seen_idx as i32 - $items_per_page as i32) {
                        0.. => $last_seen_idx -= $items_per_page, // $last_seen_idx >= $items_per_page
                        ..=-1 => $last_seen_idx = 0, // $last_seen_idx < $items_per_page
                    }
                }
                ui.separator();
                ui.monospace($items_number.to_string());
                ui.label("Number of endpoints: ");
                ui.separator();
            });
        });
    };
}

#[macro_export]
macro_rules! paginate {
    ($current_page: expr, $items_per_page: expr, $items_number: expr) => {{
        let mut range = Range {
            start: $current_page * $items_per_page,
            end: ($current_page + 1) * $items_per_page,
        };

        if range.end > $items_number {
            range.end = $items_number;
        }

        if range.start > $items_number {
            range.start = range.end - $items_per_page;
            $current_page = range.start / $items_per_page;
        }

        range
    }};
}

#[macro_export]
macro_rules! row {
    ($row: ident, $closure: expr, $(($cols:expr, $active:expr)),*) => {
        $(
            if $active {
                $row.col(|ui|{
                    if ui.add(egui::Label::new($cols).wrap(true).sense(egui::Sense::click())).clicked() {
                        $closure;
                    }
                });
            }

        )*
    }
}

#[macro_export]
macro_rules! row_openapi {
    ($row: ident, $(($cols:expr)),*) => {
        $(
            $row.col(|ui|{
                ui.add(egui::Label::new($cols).wrap(true).sense(egui::Sense::click()));
            });

        )*
    }
}

#[macro_export]
macro_rules! filter {
    ($item: expr, $filter: expr, $filter_cat: expr) => {
        match ($filter, $filter_cat) {
            (Some(f), Some(FilterCat::Host)) => $item.host().contains::<&str>(f),
            (Some(f), Some(FilterCat::Code)) => $item.status() == f.parse::<usize>().unwrap(),
            (Some(f), Some(FilterCat::Source)) => $item.remote_addr().contains::<&str>(f),
            (Some(f), Some(FilterCat::Path)) => $item.uri().contains::<&str>(f),
            (Some(f), Some(FilterCat::Generic)) => {
                $item.raw().contains::<&str>(f) | $item.response().contains::<&str>(f)
            }
            (None, _) => true,
            (Some(f), None) => $item.host().contains::<&str>(f),
        }
    };
}
