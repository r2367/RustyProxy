#[derive(Debug)]
pub enum FilterCat {
    Host,
    Code,
    Source,
    Path,
    Generic,
}
