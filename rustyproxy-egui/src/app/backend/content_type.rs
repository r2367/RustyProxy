use linked_hash_map::LinkedHashMap;
use serde_json::Value;

#[derive(serde::Deserialize, serde::Serialize, Debug, Clone, PartialEq)]
pub enum ContentType {
    Json,
    Other(String),
    Empty,
}

impl ContentType {
    pub fn from_content_type_s(s: &str) -> Self {
        if s.to_lowercase().contains("application/json") {
            Self::Json
        } else if s.is_empty() {
            Self::Empty
        } else {
            Self::Other(s.to_string())
        }
    }

    pub fn from_raw_request(s: &str) -> Self {
        let s = s.replace("\\r\\n", "\r");
        for header in s.split("\r\n").take_while(|s| !s.is_empty()) {
            let key = header.to_lowercase();
            if key.starts_with("content-type") {
                if let Some(value_raw) = header.split(':').nth(1) {
                    return Self::from_content_type_s(value_raw);
                }
            }
        }

        Self::Empty
    }

    pub fn pretty_json(data: &str) -> Result<String, Box<dyn std::error::Error>> {
        let intermediary_obj = serde_json::from_str::<LinkedHashMap<String, Value>>(data)?;
        Ok((serde_json::to_string_pretty(&intermediary_obj)?).to_string())
    }

    pub fn pretty_request(req: &str) -> Result<String, Box<dyn std::error::Error>> {
        if let Some(data) = req.split("\\r\\n\n\\r\\n\n").nth(1) {
            Ok(format!(
                "{}\\r\\n\n\\r\\n\n{}",
                req.split("\\r\\n\n\\r\\n\n").nth(0).unwrap(), //its ok to unwrap() cauz it should always be Some()
                Self::pretty_json(data)?
            ))
        } else {
            Err("nth(1) was not Some()".into())
        }
    }

    pub fn flatten_json(data: &str) -> Result<String, Box<dyn std::error::Error>> {
        let intermediary_obj = serde_json::from_str::<LinkedHashMap<String, Value>>(data)?;
        Ok((serde_json::to_string(&intermediary_obj)?).to_string())
    }

    pub fn flatten_request(req: &str) -> Result<String, Box<dyn std::error::Error>> {
        if let Some(data) = req.split("\\r\\n\n\\r\\n\n").nth(1) {
            Ok(format!(
                "{}\\r\\n\n\\r\\n\n{}",
                req.split("\\r\\n\n\\r\\n\n").nth(0).unwrap(), //its ok to unwrap() cauz it should always be Some()
                Self::flatten_json(data)?
            ))
        } else {
            Err("nth(1) was not Some()".into())
        }
    }
}
