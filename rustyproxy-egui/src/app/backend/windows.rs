use crate::app::mods::{
    browser_settings::BrowserSettings, inspector::Inspector, interact_sh::Interact,
    notes::NoteViewer, openapi::OpenApiWindow, websockets::WebSocketViewer,
};

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct Window {
    pub name: String,
    pub is_active: bool,
    pub wtype: Wtype,
    pub api_addr: Option<String>,
    pub api_port: Option<usize>,
    pub api_secret: Option<String>,
    pub proxy_addr: Option<String>,
    pub proxy_port: Option<usize>,
    pub clicked: bool,
    pub is_remote: bool,
    pub enable_paging: bool,
}

impl Default for Window {
    fn default() -> Self {
        Self {
            is_active: true,
            name: String::new(),
            wtype: Wtype::default(),
            api_addr: None,
            api_port: None,
            api_secret: None,
            proxy_addr: None,
            proxy_port: None,
            clicked: false,
            is_remote: true,
            enable_paging: true,
        }
    }
}

impl Window {}

#[derive(Debug, serde::Deserialize, serde::Serialize, Default)]
pub enum Wtype {
    #[default]
    Proxy,
    History,
    Inspector(Inspector),
    Settings,
    WebSocketViewer(WebSocketViewer),
    Memory,
    NoteViewer(NoteViewer),
    Interactsh(Interact),
    BrSettings(BrowserSettings),
    OpenApi(OpenApiWindow),
}
