use crate::app::backend::dbutils;
use reqwest::header::{HeaderMap, HeaderName, HeaderValue};

pub fn get_new_from_last_id(
    last_id: usize,
    url: &str,
    secret: &str,
) -> Result<String, reqwest::Error> {
    let mut headers = HeaderMap::new();
    headers.insert(
        HeaderName::from_bytes(b"rp_auth").unwrap(),
        HeaderValue::from_bytes(secret.to_string().as_bytes()).unwrap(),
    );
    let cli = reqwest::blocking::Client::builder()
        .danger_accept_invalid_certs(true)
        .default_headers(headers)
        .timeout(None) // Temporary fix before having the paging working.
        .build()?;
    let r = cli
        .get(format!("https://{}/api/requests/{}", url, last_id))
        .send();
    match r {
        Ok(r) => r.text(),
        Err(e) => {
            println!("{}", e);
            Err(e)
        }
    }
}

pub fn parse_result(s: String) -> Vec<dbutils::HistLine> {
    let res: Vec<dbutils::HistLine> = serde_json::from_str(&s).unwrap_or_default();
    res
}

pub fn get_new_inspectors_from_last_id(
    last_id: usize,
    url: &str,
    secret: &str,
) -> Result<String, reqwest::Error> {
    let mut headers = HeaderMap::new();
    headers.insert(
        HeaderName::from_bytes(b"rp_auth").unwrap(),
        HeaderValue::from_bytes(secret.to_string().as_bytes()).unwrap(),
    );
    let cli = reqwest::blocking::Client::builder()
        .danger_accept_invalid_certs(true)
        .default_headers(headers)
        .build()?;
    let r = cli
        .get(format!("https://{}/api/inspectors/{}", url, last_id))
        .send();
    match r {
        Ok(r) => r.text(),
        Err(e) => {
            println!("{}", e);
            Err(e)
        }
    }
}

pub fn parse_inspectors_result(s: String) -> Vec<dbutils::Inspectors> {
    let res: Vec<dbutils::Inspectors> = serde_json::from_str(&s).unwrap_or_default();
    res
}

pub fn push_inspector_to_api(
    i: &dbutils::Inspectors,
    url: &str,
    secret: &str,
) -> Result<String, reqwest::Error> {
    let mut headers = HeaderMap::new();
    headers.insert(
        HeaderName::from_bytes(b"rp_auth").unwrap(),
        HeaderValue::from_bytes(secret.to_string().as_bytes()).unwrap(),
    );
    let cli = reqwest::blocking::Client::builder()
        .danger_accept_invalid_certs(true)
        .default_headers(headers)
        .build()?;
    let r = cli
        .post(format!("https://{}/api/inspectors", url))
        .body(serde_json::to_string(i).unwrap())
        .send();

    match r {
        Ok(r) => r.text(),
        Err(e) => {
            println!("{}", e);
            Err(e)
        }
    }
}

pub fn get_new_websockets_from_last_id(
    last_id: usize,
    url: &str,
    secret: &str,
) -> Result<String, reqwest::Error> {
    let mut headers = HeaderMap::new();
    headers.insert(
        HeaderName::from_bytes(b"rp_auth").unwrap(),
        HeaderValue::from_bytes(secret.to_string().as_bytes()).unwrap(),
    );
    let cli = reqwest::blocking::Client::builder()
        .danger_accept_invalid_certs(true)
        .default_headers(headers)
        .build()?;
    let r = cli
        .get(format!("https://{}/api/websockets/{}", url, last_id))
        .send();
    match r {
        Ok(r) => r.text(),
        Err(e) => {
            println!("{}", e);
            Err(e)
        }
    }
}

pub fn parse_websockets_result(s: String) -> Vec<dbutils::WebSockets> {
    let res: Vec<dbutils::WebSockets> = serde_json::from_str(&s).unwrap_or_default();
    res
}

pub fn get_new_notes_from_last_id(
    last_id: usize,
    url: &str,
    secret: &str,
) -> Result<String, reqwest::Error> {
    let mut headers = HeaderMap::new();
    headers.insert(
        HeaderName::from_bytes(b"rp_auth").unwrap(),
        HeaderValue::from_bytes(secret.to_string().as_bytes()).unwrap(),
    );
    let cli = reqwest::blocking::Client::builder()
        .danger_accept_invalid_certs(true)
        .default_headers(headers)
        .build()?;

    let r = cli
        .get(format!("https://{}/api/notes/{}", url, last_id))
        .send();
    match r {
        Ok(r) => r.text(),
        Err(e) => {
            eprintln!("error: {}", e);
            Err(e)
        }
    }
}

pub fn parse_notes_result(s: String) -> Vec<dbutils::Note> {
    let res: Vec<dbutils::Note> = serde_json::from_str(&s).unwrap_or_default();
    res
}

pub fn push_note_to_api(
    url: &str,
    secret: &str,
    note: &dbutils::Note,
    is_new: bool,
) -> Result<String, reqwest::Error> {
    match is_new {
        true => {
            let mut headers = HeaderMap::new();
            headers.insert(
                HeaderName::from_bytes(b"rp_auth").unwrap(),
                HeaderValue::from_bytes(secret.to_string().as_bytes()).unwrap(),
            );
            let cli = reqwest::blocking::Client::builder()
                .danger_accept_invalid_certs(true)
                .default_headers(headers)
                .build()?;
            let r = cli
                .post(format!("https://{}/api/notes", url))
                .body(serde_json::to_string(note).unwrap())
                .send();

            match r {
                Ok(r) => r.text(),
                Err(e) => {
                    dbg!(&e);
                    eprintln!("err: {}", e);
                    Err(e)
                }
            }
        }
        false => patch_note_in_api(url, secret, note),
    }
}

pub fn patch_note_in_api(
    url: &str,
    secret: &str,
    note: &dbutils::Note,
) -> Result<String, reqwest::Error> {
    let mut headers = HeaderMap::new();
    headers.insert(
        HeaderName::from_bytes(b"rp_auth").unwrap(),
        HeaderValue::from_bytes(secret.to_string().as_bytes()).unwrap(),
    );
    let cli = reqwest::blocking::Client::builder()
        .danger_accept_invalid_certs(true)
        .default_headers(headers)
        .build()?;
    let r = cli
        .patch(format!("https://{}/api/notes/{}", url, note.id()))
        .body(serde_json::to_string(note).unwrap())
        .send();

    match r {
        Ok(r) => r.text(),
        Err(e) => {
            println!("{}", e);
            Err(e)
        }
    }
}

pub fn download_cert_from_api(url: &str, secret: &str) -> Result<String, reqwest::Error> {
    let mut headers = HeaderMap::new();
    headers.insert(
        HeaderName::from_bytes(b"rp_auth").unwrap(),
        HeaderValue::from_bytes(secret.to_string().as_bytes()).unwrap(),
    );
    let cli = reqwest::blocking::Client::builder()
        .danger_accept_invalid_certs(true)
        .default_headers(headers)
        .build()?;
    let r = cli.get(format!("https://{}/cert", url)).send();

    match r {
        Ok(r) => r.text(),
        Err(e) => {
            eprintln!("{}", e);
            Err(e)
        }
    }
}
