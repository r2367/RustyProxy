use crate::app::mods::inspector::Inspector;
use rusqlite::{Connection, Error};
use std::path::Path;

pub fn _try_open_conn(projectpath: &str) -> Result<Connection, Error> {
    let fpath = format!("{}/hist.db", projectpath);
    Connection::open(fpath)
}

pub fn _is_valid_project_path(fpath: &String) -> bool {
    if Path::new(&fpath).exists() {
        return _try_open_conn(fpath).is_ok();
    }

    false
}

#[derive(serde::Deserialize, serde::Serialize, Debug, Clone)]
pub struct HistLine {
    id: usize,
    remote_addr: String,
    uri: String,
    method: String,
    params: bool,
    status: usize,
    size: usize,
    raw: String,
    ssl: bool,
    response: String,
    response_time: String,
    host: String,
    timestamp: String,
    content_length: usize,
}

impl HistLine {
    pub fn id(&self) -> usize {
        self.id
    }

    pub fn remote_addr(&self) -> &String {
        &self.remote_addr
    }

    pub fn uri(&self) -> &String {
        &self.uri
    }

    pub fn method(&self) -> &String {
        &self.method
    }

    pub fn status(&self) -> usize {
        self.status
    }

    pub fn size(&self) -> usize {
        self.size
    }

    pub fn raw(&self) -> &String {
        &self.raw
    }

    pub fn ssl(&self) -> bool {
        self.ssl
    }

    pub fn response(&self) -> &String {
        &self.response
    }

    pub fn response_time(&self) -> &String {
        &self.response_time
    }

    pub fn host(&self) -> &String {
        &self.host
    }

    pub fn timestamp(&self) -> &String {
        &self.timestamp
    }

    pub fn params(&self) -> bool {
        self.params
    }

    pub fn content_length(&self) -> usize {
        self.content_length
    }

    pub fn _from_histline_opt(histline_opt: Option<HistLine>) -> Self {
        let histline = histline_opt.unwrap();
        Self {
            id: histline.id(),
            remote_addr: histline.remote_addr().to_string(),
            uri: histline.uri().to_string(),
            method: histline.method().to_string(),
            params: histline.params(),
            status: histline.status(),
            size: histline.size(),
            raw: histline.raw().to_string(),
            ssl: histline.ssl(),
            response: histline.response().to_string(),
            response_time: histline.response_time().to_string(),
            host: histline.host().to_string(),
            timestamp: histline.timestamp().to_string(),
            content_length: histline.content_length(),
        }
    }

    pub fn _from_histline_ref_opt(histline_opt: Option<&HistLine>) -> Self {
        let histline = histline_opt.unwrap();
        Self {
            id: histline.id(),
            remote_addr: histline.remote_addr().to_string(),
            uri: histline.uri().to_string(),
            method: histline.method().to_string(),
            params: histline.params(),
            status: histline.status(),
            size: histline.size(),
            raw: histline.raw().to_string(),
            ssl: histline.ssl(),
            response: histline.response().to_string(),
            response_time: histline.response_time().to_string(),
            host: histline.host().to_string(),
            timestamp: histline.timestamp().to_string(),
            content_length: histline.content_length(),
        }
    }

    pub fn from_histline_ref(histline: &HistLine) -> Self {
        Self {
            id: histline.id(),
            remote_addr: histline.remote_addr().to_string(),
            uri: histline.uri().to_string(),
            method: histline.method().to_string(),
            params: histline.params(),
            status: histline.status(),
            size: histline.size(),
            raw: histline.raw().to_string(),
            ssl: histline.ssl(),
            response: histline.response().to_string(),
            response_time: histline.response_time().to_string(),
            host: histline.host().to_string(),
            timestamp: histline.timestamp().to_string(),
            content_length: histline.content_length(),
        }
    }
}

#[derive(serde::Deserialize, serde::Serialize, Debug, Clone)]
pub struct Inspectors {
    pub id: usize,
    pub request: String,
    pub response: String,
    pub modified_request: String,
    pub new_response: String,
    pub ssl: bool,
    pub target: String,
    pub bf_results: Vec<(
        usize,
        std::string::String,
        std::string::String,
        std::string::String,
        std::string::String,
        std::time::Duration
    )>,
    pub bf_request: String,
}

impl Inspectors {
    pub fn from_inspector(h: &Inspector) -> Self {
        Self {
            id: 0,
            request: h.request.to_string(),
            response: h.response.to_string(),
            modified_request: h.modified_request.to_string(),
            new_response: h.new_response.to_string(),
            ssl: h.ssl,
            target: h.target.to_string(),
            bf_results: h.bf_results.clone(),
            bf_request: h.bf_request.to_string(),
        }
    }
}

#[derive(serde::Deserialize, serde::Serialize, Debug, Clone)]
pub struct WebSockets {
    pub id: usize,
    pub stream_rep: String,
    pub speaker: String,
    pub ssl: bool,
    pub data: String,
}

impl WebSockets {
    pub fn host(&self) -> &str {
        &self.speaker
    }
}

#[derive(Default, Debug, serde::Deserialize, serde::Serialize, Clone)]
pub struct Note {
    id: usize,
    body: String,
    title: String,
}

impl Note {
    pub fn body(&self) -> &str {
        self.body.as_ref()
    }

    pub fn body_mut(&mut self) -> &mut String {
        &mut self.body
    }

    pub fn id(&self) -> usize {
        self.id
    }

    pub fn title_mut(&mut self) -> &mut String {
        &mut self.title
    }

    pub fn set_id(&mut self, id: usize) {
        self.id = id;
    }
}
