use super::apiutils;
use super::dbutils;
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

#[derive(PartialEq)]
pub enum Task {
    PollNotes,
    PollHistories,
    PollWebsockets,
    PollInspectors,
}

impl Task {
    fn possibilities() -> &'static [Task] {
        &[
            Self::PollHistories,
            Self::PollInspectors,
            Self::PollNotes,
            Self::PollWebsockets,
        ]
    }
}
impl Default for Task {
    fn default() -> Self {
        Self::PollHistories
    }
}

#[derive(Debug)]
pub enum TaskResult {
    Histories(Vec<dbutils::HistLine>, usize),
    Inspectors(Vec<dbutils::Inspectors>, usize),
    Websockets(Vec<dbutils::WebSockets>, usize),
    Notes(Vec<dbutils::Note>, usize),
}

impl Default for TaskResult {
    fn default() -> Self {
        Self::Histories(vec![], 0)
    }
}

#[derive(Default)]
pub struct Worker<TaskResult> {
    tx: Option<mpsc::Sender<TaskResult>>,
    last_history_id: usize,
    last_inspector_id: usize,
    last_note_id: usize,
    last_websocket_id: usize,
    api_url: String,
    api_secret: String,
}

impl Worker<TaskResult> {
    pub fn new(tx: mpsc::Sender<TaskResult>, api_url: String, api_secret: String) -> Self {
        Self {
            tx: Some(tx),
            api_url,
            api_secret,
            ..Default::default()
        }
    }

    pub fn run(&mut self) {
        let tx = self.tx.clone().unwrap();
        loop {
            for task in Task::possibilities() {
                let res = match task {
                    Task::PollHistories => {
                        let vresult = apiutils::parse_result(
                            apiutils::get_new_from_last_id(
                                self.last_history_id,
                                &self.api_url,
                                &self.api_secret,
                            )
                            .unwrap_or_default(),
                        );
                        if vresult.len() == 0 {
                            continue;
                        }
                        let last_id = vresult.iter().max_by_key(|x| x.id()).unwrap().id();
                        self.last_history_id = last_id;
                        TaskResult::Histories(vresult, last_id)
                    }
                    Task::PollInspectors => {
                        let vresult = apiutils::parse_inspectors_result(
                            apiutils::get_new_inspectors_from_last_id(
                                self.last_inspector_id,
                                &self.api_url,
                                &self.api_secret,
                            )
                            .unwrap_or_default(),
                        );
                        if vresult.len() == 0 {
                            continue;
                        }
                        let last_id = vresult.iter().max_by_key(|x| x.id).unwrap().id;
                        self.last_inspector_id = last_id;
                        TaskResult::Inspectors(vresult, last_id)
                    }
                    Task::PollNotes => {
                        let vresult = apiutils::parse_notes_result(
                            apiutils::get_new_notes_from_last_id(
                                self.last_note_id,
                                &self.api_url,
                                &self.api_secret,
                            )
                            .unwrap_or_default(),
                        );
                        if vresult.len() == 0 {
                            continue;
                        }
                        let last_id = vresult.iter().max_by_key(|x| x.id()).unwrap().id();
                        self.last_note_id = last_id;
                        TaskResult::Notes(vresult, last_id)
                    }
                    Task::PollWebsockets => {
                        let vresult = apiutils::parse_websockets_result(
                            apiutils::get_new_websockets_from_last_id(
                                self.last_websocket_id,
                                &self.api_url,
                                &self.api_secret,
                            )
                            .unwrap_or_default(),
                        );
                        if vresult.len() == 0 {
                            continue;
                        }
                        let last_id = vresult.iter().max_by_key(|x| x.id).unwrap().id;
                        self.last_websocket_id = last_id;
                        TaskResult::Websockets(vresult, last_id)
                    }
                };

                tx.send(res).unwrap();
            }
            thread::sleep(Duration::from_millis(1000));
        }
    }
}
