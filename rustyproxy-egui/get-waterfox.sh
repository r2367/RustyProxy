#!/bin/bash


echo "[i] Checking for waterfox binaries"
if [ ! -d "./waterfox" ]; then
    echo "[i] Downloading waterfox browser archive"
    [ ! -f "./waterfox.tar.zstd" ] && wget https://waterfox-rustyproxy.s3.eu-west-3.amazonaws.com/waterfox.tar.zstd
    echo "[+] Waterfox.tar.zstd acquired, extracting"
    zstd -d ./waterfox.tar.zstd --stdout | tar xvf - > /dev/null
    echo "[+] Done with extracting"
fi

echo "[i] Checking for waterfox profile"
if [ ! -d "./waterfox_profile" ]; then
    echo "[i] Downloading waterfox profile archive"
    [ ! -f "./waterfox_profile.tar.zstd" ] && wget https://waterfox-rustyproxy.s3.eu-west-3.amazonaws.com/waterfox_profile.tar.zstd
    echo "[+] waterfox_profile.tar.zstd acquired, extracting"
    zstd -d ./waterfox_profile.tar.zstd --stdout | tar xvf - > /dev/null
    echo "[+] Done with extracting"
fi

echo "[i] Over."